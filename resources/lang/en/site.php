<?php

return[
    'into' => "Republic of Uzbekistan <br/>  Ministry of Agriculture",
    'adress' => "Our address",
    'lang' => "EN",

    //'search' => "Enter the name of the service",

    'admin' => "Order service",

    'header' => "HOME",
   'header1' => "TESTS",
   'header2' => "CERTIFICATION",
   'header3' => "REGISTRY",
   'header4' => "NORMATIVE BASE",
   'header5' => "FREQUENTLY ASKED QUESTIONS",
   'header6' => "CONTACTS",

   'card' => "Joint support and <br/>  connectivity",
   'card1' => "Phone CALL center:",
   'card2' => "List of tested agricultural <br/> technicians ",

   'services' => "How to get the service",
   'services1' => "Apply to test farming and reclamation practices and track the status of your application.",
   'services2' => "Check out the basic calculation.",
   'services3' => "Cancel the application if the initial settlement amount does not suit you.",
   'services4' => "Sign the electronic contract.",
   'services5' => "Make an initial payment on a contractual basis.",
   'services6' => "Try this technique.",

   'year' => "22 September, Tuesday",

   'round' => "Accepted",
   'round1' => "Completed",
   'round2' => "Rejected",
   'round3' => "On performance",
   'round4' => "Expired",

   'row' => "REGULATORY BASE OF RIGHTS",

   'btn' => "President of the Republic of Uzbekistan May 23, 2013 “On measures to further improve the testing system of agricultural machinery and equipment“",
   'btn1' => "Resolution No. PQ-1972",
   'btn2' => "President of the Republic of Uzbekistan April 17, 2019 ”On measures to improve the system of public administration in agriculture”",
   'btn3' => "Resolution PF-5708",
   'btn4' => "More",
   'btn5' => "More",
   'btn6' => "President of the Republic of Uzbekistan June 27, 2020 ”On measures to improve the testing and certification system of agricultural and land reclamation equipment”",
   'btn7' => "Resolution No. PQ-4760",
   'btn8' => "More",
   'btn9' => "Resolution of the Cabinet of Ministers of the Republic of Uzbekistan dated December 15, 2020 “On measures to further improve the activities of the Center for certification and testing of agricultural machinery and technologies under the Ministry of Agriculture of the Republic of Uzbekistan“ ",
   'btn10' => "Resolution No. 785",
   'btn11' => "More",

   'row1' => "MAIN FUNCTIONS OF THE CENTER",

   'comp' => "Agriculture and food supply of the Ministry of Agriculture of the Republic of Uzbekistan
   agriculture and reclamation equipment, modern resource-saving crop cultivation
   new standards, methodological documents and procedures in the field of technology testing
   development and revision of existing ones;",
   'comp1' => "Agriculture and land reclamation in order to decide on serial production testing of equipment, experimental samples of their components and assemblies;",
   'comp2' => "Ability to control the stability of product quality and continue its production testing of serially produced agricultural and reclamation equipment in order to determine broadcast;",
   'comp3' => "Taking into account the peculiarities of agricultural production in the republic. including agricultural and reclamation methods to determine the effectiveness of the application conducting other types of tests of foreign production;",
   'comp4' => "Agriculture using other new types of agricultural techniques and reclamation technologies
   approbation of modern resource-saving crop production technologies;",
   'comp5' => "International standards for describing agricultural and land reclamation equipment in foreign and domestic countries.
   and to determine compliance with regulatory documents in the field of technical regulation
   Carrying out certification works and tests in the manner prescribed by law;",

   'footer'  =>"Ministry of Agriculture of the Republic of Uzbekistan",
   'footer1' => "Agricultural machinery of the Ministry of Agriculture of the Republic of Uzbekistan
   and technology certification and testing center",
   'footer2' => "Address",
   'footer3' => "110800, Tashkent region, Yangiyul district, Gulbahor village, st. Yoshlik, 5.",
   'footer4' => "Phone:",
   'footer5' => "State tests",
   'footer6' => "Types of tests",
   'footer7' => "The purpose of the test",
   'footer8' => "Test results",
   'footer9' => "Required documents",
   'footer10' => "Main menu",
   'footer11' => "Main functions",
   'footer12' => "Normative documents",
   'footer13' => "Register",
   'footer14' => "Composition",
   'footer15' => "Connection",
   'footer16' => "Copyright 2021 - Center for certification and testing of agricultural machinery and technologies. All rights reserved.",

   'section' => "Read more",
   'section1' => "We do it better ",
   'section2' => "for",






];



