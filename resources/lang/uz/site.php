﻿<?php
return[
    'into'   => "O‘zbekiston Respublikasi <br/> Qishloq xo‘jaligi vazirligi",
    'adress' => "Bizning manzil",
    'lang'   => "UZ",

   //'search' => "Хизмат номини киритинг",

   'admin'   => "Buyurtma xizmati",

    'header' => "BOSH SAHIFA",
   'header1' => "TESTLAR",
   'header2' => "SERTIFIKATLAR",
   'header3' => "QAYTA TIKLASH",
   'header4' => "NORMATIV BAZA",
   'header5' => "KO‘P SO‘RALADIGAN SAVOLLAR",
   'header6' => "ALOQA",

   'card'    => "Qo‘llab kuvvatlash va boglanish",
   'card1'   => "Telefon qo‘ng‘iroqlari markazi:",
   'card2'   => "Sinovdan o‘tkazilayotgan <br/> agro texnikalar ro‘yxati",

   'services'  => "Xizmatni qanday olish mumkin",
   'services1' => "Kishlok xo‘jaligi va meliorasiya texnikasini sinovdan o‘tkazish uchun ariza bering va ariza statusini kuzatib boring.",
   'services2' => "Birlamchi kalkulyasiya bilan tanishib chiking.",
   'services3' => "Birlamchi kalkulyasiya summasi sizni koniktirmasa ariza berishni bekor kiling.",
   'services4' => "Elektron shartnomani imzolang.",
   'services5' => "Shartnoma asosida birlamchi to‘lovni o‘tkazing.",
   'services6' => "Texnikani Sinovga topshiring.",

//    'year'    => "22 Сентябр, Cешанба",

   'round'  => "Qabul qilingan",
   'round1' => "Bajarildi",
   'round2' => "Rad etildi",
   'round3' => "Ishlash haqida",
   'round4' => "Muddati tugagan",

   'row'    => "NORMATIV HUQUQLAR BAZASI",

   'btn'    => "O‘zbekiston Respublikasi Prezidentining 2013 yil 23 maydagi “Qishloq xo‘jaligi texnika va texnologiyalari sinash tizimini yanada takomillashtirish chora-tadbirlari to‘g‘risida”",
   'btn1'   => "PQ-1972-son Qarori",
   'btn2'   => "O‘zbekiston Respublikasi Prezidentining 2019 yil 17 aprel “Qishloq xo‘jaligi sohasida davlat boshqaruvi tizimini takomillashtirish chora-tadbirlari to‘g‘risida”",
   'btn3'   => "PF-5708-son Farmoni",
   'btn4'   => "Batafsil",
   'btn5'   => "Batafsil",
   'btn6'   => "O‘zbekiston Respublikasi Prezidentining 2020 yil 27 iyundagi “Qishloq xo‘jaligi va meliorasiya texnikalarini sinash va syertifikatlash tizimini takomillashtirish chora-tadbirlari to‘g‘risida”",
   'btn7'   => "PQ-4760-son Qarori",
   'btn8'   => "Batafsil",
   'btn9'   => "O‘zbekiston Respublikasi Vazirlar Mahkamasining 2020 yil 15 dekabrdagi “O‘zbekiston Respublikasi Qishloq xo‘jaligi vazirligi huzuridagi Qishloq xo‘jaligi texnikasi va texnologiyalarini syertifikatlash va sinash markazi faoliyatini yanada takomillashtirish chora-tadbirlari to‘g‘risida”",
   'btn10'  => "785-son Qarori",
   'btn11'  => "Batafsil",

   'row1'   => "MARKAZNING ASOSIY VAZIFALARI",

   'comp'   => "O‘zbekiston Respublikasi Qishloq xo‘jaligi vazirligi huzuridagi Qishloq xo‘jaligi va oziq-ovqat ta’minoti
   ilmiy-ishlab chiqarish markazining soha ilmiy tadqiqot institutlari bilan birgalikda qishloq xo‘jaligi va
   meliorasiya texnikasi, qishloq xo‘jaligi ekinlarini etishtirishning resurslarni tejaydigan zamonaviy
   texnologiyalari sinovlarini o‘tkazish sohasida yangi standartlar, uslubiy hujjatlar va tartibotlarni
   ishlab chiqish va amaldagilarini qayta ko‘rib chiqish;",
   'comp1' => "Seriyalab ishlab chiqarish to‘g‘risida qaror qabul qilish maqsadida qishloq xo‘jaligi va meliorasiya
   texnikasini, ularning agregatlari va uzellarining tajriba namunalari sinovlarini o‘tkazish;",
   'comp2' => "Mahsulot sifatining barqarorligini nazorat qilish va uni ishlab chiqarishni davom ettirish imkoniyatini
   aniqlash maqsadida seriyali ishlab chiqarilayotgan qishloq xo‘jaligi va meliorasiya texnikasi sinovlarini
   o‘tkazish;",
   'comp3' => "Respublikada qishloq xo‘jaligi ishlab chiqarishining o‘ziga xos xususiyatlarini hisobga olgan holda uni
   qo‘llash samaradorligini aniqlash maqsadida qishloq xo‘jaligi va meliorasiya texnikasining, shu jumladan
   xorijda ishlab chiqarilganlarining boshqa turdagi sinovlarini o‘tkazish;",
   'comp4' => "Qishloq xo‘jaligi va meliorasiya texnikasining boshqa yangi turlarini qo‘llagan holda qishloq xo‘jaligi
   ekinlari yetishtirishning resurslarni tejaydigan zamonaviy texnologiyalari sinovlarini o‘tkazish;",
   'comp5' => "Chet el va mamlakatimiz qishloq xo‘jaligi va meliorasiya texnikasi tavsiflarining xalqaro standartlarga
   va texnik jihatdan tartibga solish sohasidagi normativ hujjatlarga muvofiqligini aniqlash maqsadida
   sertifikatlash ishlari va sinovlarini qonun hujjatlarida belgilangan tartibda o‘tkazish",

   'footer'    => "O‘zbekiston Respublikasi Qishloq xo‘jaligi vazirligi",
   'footer1'   => "O‘zbekiston Respublikasi Qishloq xo‘jaligi vazirligi huzuridagi Qishloq xo‘jaligi texnikasi
   va texnologiyalarini sertifikatlash va sinash markazi",
    'footer2'  => "Manzil",
    'footer3'  => "110800,Toshkent viloyati, Yangiyo‘l tumani, Gulbahor shaharchasi, Yoshlik ko‘chasi 5-uy.",
    'footer4'  => "Telefon:",
    'footer5'  => "Davlat sinovlari",
    'footer6'  => "Sinov turlari",
    'footer7'  => "Sinov maqsadi",
    'footer8'  => "Sinov natijalari",
    'footer9'  => "Zarur xujjatlar",
    'footer10' => "Asosiy menyu",
    'footer11' => "Asosiy vazifalar",
    'footer12' => "Meyyoriy xujjatlar",
    'footer13' => "Reestr",
    'footer14' => "Tuzilma",
    'footer15' => "Bog‘lanish",
    'footer16' => "Copyright 2021 - Qishloq xo‘jaligi texnikasi va texnologiyalarini sertifikatlash va sinash markazi. Barcha huquqlar himoyalangan.",

    'section'  => "Batafsil",
    'section1' => "Xizmatlarimiz yanada qulay",
    'section2' => "",
    'section3' => "bizning mijozlarimiz", "bizning hamjamiyat", "bizning xodimlarimiz",



    'by_pageview'               => 'Sahifa bo`yicha',
    'by_sessions'               => 'Sessiyalar bo`yicha',
    'by_users'                  => 'Foydalanuvchilar bo`yicha :count :string',
    'no_client_id'              => 'Analitikani faollashtirish uchun siz Google Analytics mijoz identifikatorini olishingiz va uni sozlamalar menyusining <code> google_analytics_client_id </code> maydoniga qo`shishingiz kerak. Google Analytics kodini oling',
    'set_view'                  => 'Ko`rinishni tanlang',
    'this_vs_last_week'         => 'Joriy hafta o`tgan haftaga nisbatan',
    'this_vs_last_year'         => 'Joriy yil o`tgan yilga nisbatan',
    'top_browsers'              => 'Eng yaxshi brauzerlar',
    'top_countries'             => 'Eng yaxshi mamlakatlar',
    'various_visualizations'    => 'Har xil vizualizatsiyalar',

    'title'                     => 'Chat',
    'info'                      => 'Ariza haqida ma\'lumot',
    'appealer'                  => 'Ariza yuboruvchi',
    'region'                    => 'Viloyat',
    'route'                     => 'Yo\'nalish',
    'sent'                      => 'Yuborilgan',
    'status'                    => 'Holati',
    'close'                     => 'Yopish',
    'send_button'               => 'Yuborish',
    'deleted_user'              => 'O\'chirilgan akkount',
    'deleted_route'             => 'O\'chirilgan',
    'deleted_action'            => 'o\'chirilgan',
    'medium'                    => 'O\'rtacha',
    'low'                       => 'Past',
    'high'                      => 'Yuqori'





];
