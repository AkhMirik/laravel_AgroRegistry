<?php

return [
    'page'				        => 'Sahifa | Sahifalar',
    'page_link_text' 	=> 'Barcha sahifalar',
    'page_text' 		    => 'Malumotlar bazasida :count',
    'post' 				       => 'ta javob |ta javoblar',
    'post_link_text' 	=> 'Barcha ma`lumotlar',
    'post_text' 		    => 'Ma`lumotlar bazasida :count ta :string bor',
    'user' 				       => 'ta foydalanuvchi | ta foydalanuvchilar',
    'user_link_text' 	=> 'Barcha foydalanuvchilar',
    'user_text' 		    => 'Ma`lumotlar bazasida :count :string',
    'buttonappeals' 		    => 'Barcha arizalar',
    'buttonanswers' 		    => 'Barcha javoblar',
    
    'buttonapps' 		    => 'Barcha arizalar',
    'application_text' 		    => 'Ma`lumotlar ba`zasida :count :string',
    'apps'           => 'arizalar',
    'Company' => 'Barcha kompaniyalar',
    'Companies' => 'kompaniyalar',
    'labaratory' => 'Barcha laboratoriyalar',
    'labaratories' => 'laboratoriya',
    'procedure' => 'Barcha jarayonlar',
    'procedure1' => 'jarayon',
    'buttonCompletedApps' 		    => 'Barcha yakullangan ariza',
    'Comapplication_text' 		    => 'Ma`lumotlar bazasida :count ta :string bor',
    'completedApps'           => 'yakullangan arizalar',
];
