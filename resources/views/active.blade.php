@extends('voyager::master')
@section('content')
    @php
        $lang = app()->getLocale();
    @endphp

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.2/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.9/css/fixedHeader.bootstrap.min.css">
    <div hidden id="lang">
        {{$lang}}
    </div>
   <div class="container">
       <table id="example" class="table table-striped table-bordered" style="width:100%">
           <thead>
           <tr>
               <th>url</th>
               <th>method</th>
               <th>ip</th>
               <th>agent</th>
           </tr>
           </thead>
           <tbody>
           @foreach($nmadr as $nm)
               <tr>
                   <td>{{$nm->url}}</td>
                   <td>{{$nm->method}}</td>
                   <td>{{$nm->ip}}</td>
                   <td>{{$nm->agent}}</td>
               </tr>
           @endforeach
           </tbody>
       </table>
   </div>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.2/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            let lang = $('#lang').text().trim();
            $('#example').DataTable({
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                "order": [[0, 'desc']],
                language: {
                    url: `https://cdn.datatables.net/plug-ins/1.11.3/i18n/${lang}.json`                }
            });
        });
    </script>
@endsection
