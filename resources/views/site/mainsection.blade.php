﻿@extends('site.layouts.app')

@section('content')
<div class="section">

    <div class="container headwrite">
        <p class="class11"><i><b>@lang('site.section1')</b><br>@lang('site.section2') <span class="typed-text"></span><span
                class="cursor">&nbsp;</span></i><br>
            <button class="button1" onClick='window.location="#gohere"'>@lang('site.section')</button>
        </p>

    </div>


    <div class="video-container">
        <video autoplay loop muted>
            <source src="{{asset('video/1.mp4')}}" type="video/mp4">
        </video>
    </div>
    <div class="overlay"></div>

    <div class="shape1"></div>
    <div class="shape2"></div>


</div>


<style>
/* .zagalovka {
    font-family: 'Courgette', cursive;
} */
.container p span.cursor {
    display: inline-block;
    background-color: #ccc;
    margin-left: 0.1rem;
    width: 3px;
    animation: blink 1s infinite;

}

.button1 {
    top: 0;
    position: relative;
    background-color: #4CAF50;
    /* Green */
    border: none;
    color: white;
    padding: 9px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 25px;
    border-radius: 5px;
    transform: skew(-10deg);
}


.shape1 {
    position: absolute;
    top: 0;
    left: 0;
    width: calc(30% + 380px);
    height: 100%;
    mix-blend-mode: overlay;
    background-color: green transparent 100%;
    background-image: url("https://www.transparenttextures.com/patterns/dark-fish-skin.png");
    background-image: linear-gradient(100deg, #106136 50%, transparent 50%)
}

.shape2 {
    position: absolute;
    top: 0;
    left: 0;
    width: calc(30% + 600px);
    height: 100%;
    mix-blend-mode: overlay;
    background-color: green transparent 50%;
    background-image: url("https://www.transparenttextures.com/patterns/dark-fish-skin.png");
    background-image: linear-gradient(100deg, #106136 60%, transparent 50%)
}

.container p span.cursor.typing {
    animation: none;
}

.container p span.cursor.typing {
    animation: none;
}

@keyframes blink {
    0% {
        background-color: #ccc;
    }

    49% {
        background-color: #ccc;
    }

    50% {
        background-color: transparent;
    }

    99% {
        background-color: transparent;
    }

    100% {
        background-color: #ccc;
    }
}

.section {
    position: relative;
    width: 100%;
    height: 80.5vh;
    display: flex;
    align-items: center;
    justify-content: center;
    overflow: hidden;
}



.headwrite {
    padding-top: -100px;
    text-align: left;
    font-size: 4rem;
    font-family: "Cookie";
    z-index: 1;
    color: white;
    text-shadow: 1px 1px grey;
}

.video-container {
    position: absolute;
    top: -20vh;
    left: 0;
    width: 100%;
}
.certificate:hover{
    transform: scale(1.5);
    transition: 1s;
}
.certificate:not( :hover ){
    transition: 1s;
}
</style>


<!-- main start -->


<script>

    // for typing animation

    const typedTextSpan = document.querySelector(".typed-text");
    const cursorSpan = document.querySelector(".cursor");

    let textArray;
    if (window.location.pathname.includes('/uz') == true) {
        textArray = ["mijozlarimiz uchun", "jamiyatimiz uchun", "ishchi-xodimlarimiz uchun"];
    } else if (window.location.pathname.includes('/ru') == true) {
        textArray = ["наших клиентов", "нашего сообщества", "наших сотрудников"];
    } else if (window.location.pathname.includes('/en') == true) {
        textArray = ["our clients", "our community", "our employees"];
    }

    const typingDelay = 75;
    const erasingDelay = 25;
    const newTextDelay = 1000; // Delay between current and next text
    let textArrayIndex = 0;
    let charIndex = 0;

    function type() {
        if (charIndex < textArray[textArrayIndex].length) {
            if (!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
            typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex);
            charIndex++;
            setTimeout(type, typingDelay);
        } else {
            cursorSpan.classList.remove("typing");
            setTimeout(erase, newTextDelay);
        }
    }

    function erase() {
        if (charIndex > 0) {
            if (!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
            typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex - 1);
            charIndex--;
            setTimeout(erase, erasingDelay);
        } else {
            cursorSpan.classList.remove("typing");
            textArrayIndex++;
            if (textArrayIndex >= textArray.length) textArrayIndex = 0;
            setTimeout(type, typingDelay + 1100);
        }
    }

    document.addEventListener("DOMContentLoaded", function() { // On DOM Load initiate the effect
        if (textArray.length) setTimeout(type, newTextDelay + 250);
    });

</script>


<style>
@import url('https://fonts.googleapis.com/css2?family=Courgette&display=swap');



.entrancebgimg {
    position: relative;
    background-image: url("{{asset('images/layout/banner.gif')}}");
    background-repeat: no-repeat;
    background-size: cover;
    width: 100%;
    height: 800px;
}

.banner {
    margin: 140px 107px;
}

.banner p {
    width: 770px;
    font-weight: 400;
    font-size: 64px;
    color: #fff;

}

.intro-text {
    width: 238px !important;
    height: 29px;
    font-size: 24px !important;
    background: #42BC18;
    color: #FFFFFF;
    border-radius: 5px;
}

.banner span {
    font-weight: 600;
    font-size: 72px;
    color: #fff;
}

.banner-btn {
    display: block;
    width: 139px;
    height: 46.26px;
    background: #42BC18;
    border-radius: 5px;
    border: none;
    color: #fff;
    font-size: 24px;
}

.row {
    --bs-gutter-x: 0;
}
.base-div{
    position:relative;
}
.btn-div{
    position:absolute;
    bottom:10%;
}

</style>





<div class="container-fluid" id="gohere">
    <div class="row mb-5" style="--bs-gutter-x: 0;">
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mt-4 text-center">
            <div class="col-12 text-center">
                <div class="card"
                    style="width: 284px;  margin:20px; text-align:center; display:inline-block;">
                    <h5 style="font-style:'Montserrat';font-weight:500; padding-top: 15px">@lang('site.card')</h5>
                    <hr style="width:200px; margin-left:40px;">
                    <div class="card-body">
                        <h5 class="card-title" style="font-weight:400;margin-top:0;"> @lang('site.card1')</h5>
                        <p class="card-text"><img src="{{asset('images/layout/phoneicon.png')}}">+998(90) 955 55 55 <br>
                            <img src="{{asset('images/layout/phoneicon.png')}}">+998(90) 955 55 55 <br> <br> <img
                                src="{{asset('images/layout/mailicon.png')}}">uzmis1@mail.ru
                            <br> <br>
                            <div class="social-icons">
                                <a href="" target="blank">
                                    <img src="{{asset('images/layout/instagramicon.png')}}"> &nbsp;
                                </a>
                                <a href="" target="blank">
                                    <img src="{{asset('images/layout/youtubeicon.png')}}"> &nbsp;
                                </a>
                                <a href="" target="blank">
                                <img src="{{asset('images/layout/facebookicon.png')}}"> &nbsp;
                                </a>
                                <a href="" target="blank">
                                    <img src="{{asset('images/layout/telegramicon.png')}}">
                                </a>
                            </div>


                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center">
                <div class="card"
                    style="width: 284px;  margin:20px; text-align:center;display:inline-block;">
                    <h5 style="margin-top:15px;font-style:'Montserrat';font-weight:500;">@lang('site.card2')</h5>
                    <div class="card-body">
                        <h5 style="color: #42BC18;">{{date('Y')}}<h5>
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img class="d-block w-100" src="{{asset('images/layout/slideimage.png')}}"
                                                alt="First slide">

                                        </div>

                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                        data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only"></span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                        data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only"></span>
                                    </a>
                                </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 services_container mt-5">
            <div class="services">
                <h3 class="text-center mb-5">@lang('site.services')</h3>
                <div class="service_block row">
                    <div class="service col-6 text-center">
                        <img src="{{asset('assets/icons/services_layer/1.png')}}" alt="tag">
                        <p class="p-1"> @lang('site.services1')</p>
                    </div>
                    <div class="service col-6 text-center">
                        <img src="{{asset('assets/icons/services_layer/2.png')}}" alt="tag">
                        <p class="p-2"> @lang('site.services2')</p>
                    </div>
                </div>
                <div class="service_block row">
                    <div class="service col-6 text-center">
                        <img src="{{asset('assets/icons/services_layer/x.png')}}" alt="tag">
                        <p>@lang('site.services3')</p>
                    </div>
                    <div class="service col-6 text-center">
                        <img src="{{asset('assets/icons/services_layer/3.png')}}" alt="tag">
                        <p>@lang('site.services4')</p>
                    </div>
                </div>
                <div class="service_block row">
                    <div class="service col-6 text-center">
                        <img src="{{asset('assets/icons/services_layer/4.png')}}" alt="tag">
                        <p>@lang('site.services5')</p>
                    </div>
                    <div class="service col-6 text-center">
                        <img src="{{asset('assets/icons/services_layer/5.png')}}" alt="tag">
                        <p>@lang('site.services6')</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mt-5"
            style="margin-top:20px;margin-left:0px;text-align:center;">
            <div class="row">
                <div class="col-12 mb-5">
                    <img class="certificate" src="img/Rectangle 210.png" alt="" width="150px">
                </div>
                <div class="col-12">
                    <img class="certificate" src="img/Rectangle 210.png" alt="" width="150px">
                </div>
            </div>


            <!-- <div class="col-12" style="margin-top:100px">
                <h1 id="ye">{{date('Y')}}</h1>
                <h4 id="sana">@lang('site.year')</h4>
                <h1 id="mainClock">{{date('H:m')}}</h1>
            </div> -->

        </div>
    </div>
</div>
<style>
.rounddd {
    padding: 0px 0;
    border: 10px solid #42BC18;
    border-radius: 50%;
    color: #40B019;
    font-size: 41px;
    font-weight: 700;
    width: 100px;
    height: 100px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: auto;
    margin-bottom: 10px;
}
</style>

<div id="round" class="row justify-content-center mb-4 text-center" style="--bs-gutter-x: 0;">
    <div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">
        <div class="rounddd">
            <h4 class="mb-0">{{ App\Models\Application::count() }}</h4>
            <!-- bular hali qilinmagan -->
        </div>
        <p>@lang('site.round')</p>
    </div>
    <div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">
        <div class="rounddd">
            <h4 class="mb-0">0</h4>
        </div>
        <p>@lang('site.round1')</p>
    </div>
    <div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">
        <div class="rounddd">
            <h4 class="mb-0">0</h4>
        </div>
        <p>@lang('site.round2')</p>
    </div>
    <div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">
        <div class="rounddd">
            <h4 class="mb-0">0</h4>
        </div>
        <p>@lang('site.round3')</p>
    </div>
    <div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">
        <div class="rounddd">
            <h4 class="mb-0">0</h4>
        </div>
        <p>@lang('site.round4')</p>
    </div>
</div>



<!-- low start -->
<div id="baza" class="container-fluid">
    <div class="row">
        <div class="col-12 title text-center">
            <h2 class="mb-3">@lang('site.row')</h2>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 mb-3">
            <div class="low p-3 base-div">
                <p>@lang('site.btn')<a href="#">@lang('site.btn1')</a></p>

                <div class="btn-div">
                    <button type="button" class="btn">@lang('site.btn4')&#62;</button>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
                <div class="img">
                    <img src="img/justice-scale1.png" alt="img error">
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6  mb-3">
            <div class="low p-3 base-div">
                <p>@lang('site.btn2')
                    <a href="#">@lang('site.btn3')</a>
                </p>

                <div class="btn-div">
                    <button type="button" class="btn ">@lang('site.btn5') &#62;</button>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
                <div class="img">
                    <img src="img/justice-scale1.png" alt="img error">
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6  mb-3">
            <div class="low p-3 base-div">
                <p>@lang('site.btn6')
                    <a href="#">@lang('site.btn7') </a>
                </p>

                <div class="btn-div">
                    <button type="button" class="btn ">@lang('site.btn8') &#62;</button>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
                <div class="img">
                    <img src="img/justice-scale1.png" alt="img error">
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6  mb-3">
            <div class="low p-3 base-div">
                <p>@lang('site.btn9')
                    <a href="#">@lang('site.btn10')</a>
                </p>

                <div class="btn-div">
                    <button type="button" class="btn">@lang('site.btn11') &#62;</button>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
                <div class="img">
                    <img src="img/justice-scale1.png" alt="img error">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- low end -->

<!-- Markazning asosiy fazifalari start -->
<div id="vazifa" class="container-fluid">
    <div class="row">
        <div class="col-12 text-center title">
            <h2>@lang('site.row1')</h2>
            <p></p>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-3">
            <div class="comp">
                <p>@lang('site.comp')</p>
                <button type="button" class="btn">1</button>
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-3">
            <div class="comp">
                <p>@lang('site.comp1')</p>
                <button type="button" class="btn">2</button>
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-3">
            <div class="comp">
                <p>@lang('site.comp2')</p>
                <button type="button" class="btn">3</button>
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-3">
            <div class="comp">
                <p>@lang('site.comp3')</p>
                <button type="button" class="btn">4</button>
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-3">
            <div class="comp">
                <p>@lang('site.comp4')</p>
                <button type="button" class="btn">5</button>
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-3">
            <div class="comp">
                <p>@lang('site.comp5')</p>
                <button type="button" class="btn">6</button>
            </div>
        </div>
    </div>
</div>
</script>

@endsection
