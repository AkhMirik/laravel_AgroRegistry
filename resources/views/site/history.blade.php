@extends('site.layouts.app')
@section('content')

<style>
    .about {
    background: #ffffff;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.03);
    border-radius: 5px;
    padding: 20px;
    color: #555;
    font-size: 18px;
    /* line-height: 23px; */
}


section {
    width: 100%;
    height: 100%;
    background-image: url('/assets/image/group265.png');
    background-position: right;
    /* background-repeat: no-repeat; */
    background-repeat: repeat-y;
    background-size: 40%;
    
}


section h4 {
    color: #333;
    font-size: 21px;
    margin-right: 10px;
} 
.centerimage {
    height: 450px;
    padding-right: 0;
    padding-left: 0;
    background: url('/assets/image/header.png') no-repeat center center;
    display: flex;
    justify-content: center;
    align-items: center;
}

.centerimage h2 {
    font-size: 50px;
    color: #fff;
    font-weight: 500;
    text-align: center;
}

/* media start */
/* col-xl */ @media(min-width: 1200px){}
/* col-lg */ @media(min-width: 992px) and (max-width:1199px){}
/* col-md */ @media(min-width: 768px) and (max-width:991px){
    .about p {
        font-size: 16px;
    }
}
/* col-sm */ @media(min-width: 576px) and (max-width:767px){
    .about p {
        font-size: 13px;
    }
}
/* col */ @media(max-width: 575px){
    .about p {
        font-size: 13px;
    }
}
/* media end */
</style>
   
<div class="container-fluid  mb-3">
        <div class="row">
            <div class="col-12 centerimage">
                <h2>Марказ тўғрисида  тарихий <br> маълумот</h2>
            </div>
        </div>

    </div>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>1948</h4>
                    <div class="about">
                        <p>СССР Вазирлар (Кенгаши) Маҳкамасининг   26 август 1948 йилдаги № 2046 сонли фармонига асосан, СССР Қишлоқ хўжалиги вазирининг 26 август 1948 йилдаги № 1365-сонли буйруғи билан Пахтачилик агротехникаси ва механизацияси марказий станцияси, Иттифоқ пахтачилик илмий текшириш институти (Союз НИХИ) ҳудудида Ўрта Осиё Давлат худудий Машина синаш станцияси (САМИС), Ўзбекистон ССР Тошкент вилояти, Янгийўл туманида ташкил топган.</p>
                    </div>
                </div>
                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>1949</h4>
                    <div class="about">
                        <p>1949 йил март ойидан Ўрта Осиё Давлат худудий Машина синаш станцияси (САМИС)  ўзининг юридик ва амалий фаолиятини Ўрта Осиёнинг суғориладиган ерлари шароитида қишлоқ хўжалиги техникаларини синашдан бошлаган.</p>
                    </div>
                </div>
                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>1950</h4>
                    <div class="about">
                        <p>1950 йил 22 сентябрдаги № 1658 сонли СССР Қишлоқ хўжалиги вазирининг буйруғига асосан Ўрта Осиё Давлат худудий Машина синаш станцияси (САМИС)  СССР пахтачилик вазирлиги тассаруфига ўтказилиб, 1952 йил 11 августдаги  Вазирлар Маҳкамасининг № 30434 сонли фармони ва шу вазирликнинг 1952 йил 23 августдаги № 1560 сонли буйруғига кўра Каунчи каучукли экинлар ИТ станцияси ва «Қолган» тажриба хўжаликлари унинг балансига ўтказилди.</p>
                    </div>
                </div>

                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>1954</h4>
                    <div class="about">
                        <p>1954 йил 1 февралда  Ўрта Осиё Давлат худудий Машина синаш станцияси (САМИС базасида) ҳудудида Ўзбекистон ССР қишлоқ хўжалиги вазирлигига қарашли Қизил байроқли меҳнат ордени Ўрта Осиё қишлоқ хўжалиги механизацияси ва электрификацияси ИТИ (САИМЭ) ташкил топган.</p>
                    </div>
                </div>
                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>1958</h4>
                    <div class="about">
                        <p>1958 йилдан синов ишлари кўпайиб кетганлиги сабабли, 350 га «Қолган» тажриба майдонига, СССР Қишлоқ хўжалиги вазирлиги буйруғи билан Ўзбекистон ССР Сирдарё вилояти «Малек» совхози 5847 га ер майдони ва 1966 йилдан Тошкент вилояти Қуйичирчиқ туманининг «ЎзССР 5 йиллиги» совхози 6888 га ер майдони билан САМИС тажриба хўжалигига ўтказилди. САИМЭ янги техникаларни яратиб берса, САМИС яратилган янги техникаларни синовдан ўтказиб беради.</p>
                    </div>
                </div>
                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>1961</h4>
                    <div class="about">
                        <p>1961 йилдан ЦК КПССнинг ва СССР Вазирлар Кенгашининг  қарорига биноан 20 февралдан ҳамма МССлар Бутуниттифоқ  «Союзсельхозтехника» бирлашмаси ихтиёрига ўтказилди.</p>
                    </div>
                </div>
                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>1992</h4>
                    <div class="about">
                        <p>1992 йилдан  Ўзбекистон мустакилликга  эришгандан кейин машина синаш станцияси ЎзМССга ўзагартирилди.</p>
                    </div>
                </div>
                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>1998</h4>
                    <div class="about">
                        <p>Ўзбекистон Республикаси Вазирлар Маҳкамасининг 1998 йил  5 мартдаги  97-сонли   қарорига асосан “Ўзагромашсервис” уюшмаси таркибидаги Ўзбекистон қишлоқ хўжалик техникаси ва технологияларини сертификациялаш ва синаш давлат марказ (ЎзҚТТСДМ) қайта ташкил қилинди.</p>
                    </div>
                </div>
                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>2013</h4>
                    <div class="about">
                        <p>Ўзбекистон Республикаси  Президентининг  2013 йил 23 майдаги  ПҚ-1972-сонли қарорига асосан ЎзҚТТСДМ Ўзбекистон Республикаси Вазирлар Маҳкамаси таркибига киритилди. </p>
                    </div>
                </div>
                  <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>2019</h4>
                    <div class="about">
                        <p>Ўзбекистон Республикаси Президентининг 2019 йил 17 апрелдаги Қишлоқ хўжалиги соҳасида давлат бошқаруви тизимини такомиллаштириш чора-тадбирлари тўғрисида”ги ПФ–5708-сонли  Фармони  билан Марказ Ўзбекистон Республикаси Қишлоқ хўжалиги вазирлиги ҳузуридаги Қишлоқ хўжалиги техникаси ва технологияларини сертификатлаш ва синаш маркази  (ҚТТСМ) қайта ташкил этилди.</p>
                    </div>
                </div>
                <div class="col-7 d-flex justify-content-between align-items-center mb-3">
                    <h4>2020</h4>
                    <div class="about">
                        <p>Ўзбекистон Республикаси Президентининг  2020 йил 27 июндаги “Қишлоқ хўжалиги ва мелиорация техникаларини синаш  ва сертификатлаш тизимини такомиллаштириш чора-тадбирлари тўғрисида”ги ПҚ-4760-сон қарорига мувофиқ қайта ташкил қилинди.
                            Ўзбекистон Республикаси Президентининг  2020 йил 27 июндаги “Қишлоқ хўжалиги ва мелиорация техникаларини синаш  ва сертификатлаш тизимини такомиллаштириш чора-тадбирлари тўғрисида”ги ПҚ-4760-сон қарорига мувофиқ қайта ташкил қилинди.
                            </p>
                    </div>
                </div>
              
            </div>
        </div>
    </section>
@endsection