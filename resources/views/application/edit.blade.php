

@extends('voyager::master')

<style>

    /*Copied from bootstrap to handle input file multiple*/
    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    /*Also */
    .btn-success {
        border: 1px solid #c5dbec;
        background: #d0e5f5;
        font-weight: bold;
        color: #2e6e9e;
    }
    /* This is copied from https://github.com/blueimp/jQuery-File-Upload/blob/master/css/jquery.fileupload.css */
    .fileinput-button {
        position: relative;
        overflow: hidden;
    }

    .fileinput-button input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        opacity: 0;
        -ms-filter: "alpha(opacity=0)";
        font-size: 200px;
        direction: ltr;
        cursor: pointer;
    }

    .thumb {
        height: 100px;


    }

    ul.thumb-Images li {
        width: 120px;
        float: left;
        display: inline-block;
        vertical-align: top;
        height: 120px;
        background-color: #6aa2ec;
        border-radius: 10px;
        margin: 10px;
        padding: 10px;
    }

    .img-wrap {
        position: relative;
        display: inline-block;
        font-size: 0;
        overflow: hidden;
        width: 100%;
        height: 100%;
        margin-bottom: 20px;
    }
    .FileNameCaptionStyle{
        width: 100%;
        overflow: hidden;
    }

    .img-wrap .close {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
        background-color: #d0e5f5;
        padding: 5px 2px 2px;
        color: #000;
        font-weight: bolder;
        cursor: pointer;
        opacity: 0.5;
        font-size: 23px;
        line-height: 10px;
        border-radius: 50%;
    }
    .close{
        height: 20px;
        width: 20px;
        text-align: center;
        transition: 0.2s;
    }
    .img-wrap:hover .close {
        opacity: 1;
        background-color: #ff0000;
        color: white;
    }

    .FileNameCaptionStyle {
        font-size: 12px;
    }


    body{
        color: black!important;
    }
    .slider.round {
        border-radius: 17px;
    }

    .slider.round:before {
        border-radius: 50%;
    }.switch {
         position: relative;
         display: inline-block;
         width: 30px;
         height: 17px;
     }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 13px;
        width: 13px;
        left: 2px;
        bottom: 2px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(13px);
        -ms-transform: translateX(13px);
        transform: translateX(13px);
    }
</style>
<style type="text/css">
    #regiration_form fieldset:not(:first-of-type) {
        display: none;
    }
</style>
@section('content')
    <h1 class="page-title">
        <i class=""></i>Add Application
    </h1>

    <div id=" voyager-notifications">
        <form role="form" class="form-edit-add" action="{{route('voyager.applicationnnn.create')}}"
              method="POST" enctype="multipart/form-data">
            @csrf

            <div class="page-content edit-add container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h1>Қишлоқ хўжалигидаги техникаларни синовдан ўтказишга ариза бериш </h1>

                                            <h3>Ариза берувчи шахс:    {{auth()->user()->fullname}}</h3>
                                            <h3>Ариза берувчи мақоми: {{auth()->user()->user_type}}</h3>

                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group  ">
                                                <label class="control-label" for="import">Qaysi turdagi tovar uchun?</label><br>
                                                <select class="form-control " name="user_type" onchange="usertype(this.value)" required>
                                                    <option value="0">O'zbekistonda ishlab chiqarilgan tovar</option>
                                                    <option value="1">Import/Eksport</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label" for="tnved_code">TNVED Raqami</label>
                                                <select id="tnved_code" class="form-control" name="tnved_code" required>
                                                    @foreach (\App\Models\TnvedCode::all() as $type)
                                                        <option @if($type->code == $application->tnved_code ) selected  @endif  value="{{ $type->id }}">{{ $type->code }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        @if(count(auth()->user()->companies))
                                            <div class="col-lg-4">
                                                <div class="form-group">

                                                    <label class="control-label" for="company">Tashkilot</label>
                                                    <select id="company" class="form-control" name="company_id" required>
                                                        @foreach (auth()->user()->companies as $type)
                                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                    @endif
                                    <!-- GET THE DISPLAY OPTIONS -->
                                        <div class="col-lg-4">
                                            <div class="form-group">

                                                <label class="control-label" for="name">Texnika nomi</label>
                                                <input type="text" class="form-control" name="vehicle_name" placeholder="Texnika nomi" required
                                                       value="{{$application->vehicle_name}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label" for="import_country">Импорт қилувчи давлат</label>
                                                {{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
                                                <select id="import_country" class="form-control" name="import_country" required>
                                                    @foreach (\App\Models\countries::all() as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name   }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label" for="manafactured_country">Ишлаб чиқарувчи давлат</label>
                                                {{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
                                                <select id="manafactured_country" class="form-control" name="manafactured_country" required>
                                                    @foreach (\App\Models\countries::all() as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name   }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label" for="sender_company">Товарни жўнатувчи ташкилот номи</label>
                                                <select id="sender_company" class="form-control" name="sender_company" required>
                                                    @foreach(App\Models\Gtk::all() as $gtk)
                                                        <option value="{{$gtk->G8_NAME}}" data-gtk="{{$gtk->id}}">{{$gtk->G8_NAME}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label" for="quantity">сони</label>
                                                <select id="quantity" class="form-control" name="quantity" required>
                                                    @foreach(App\Models\Tovar::all() as $tovar)
                                                        <option value="{{$tovar->QUANTITY}}">{{$tovar->QUANTITY}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="col-lg-4">
                                            <div class="form-group ">
                                                <label class="control-label" for="vehicle_category">Техника категорияси</label>
                                                {{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
                                                <select id="vehicle_category" class="form-control" name="vehicle_category" required>
                                                    @foreach (\App\Models\VehicleType::all() as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label" for="vehicle_category_type">Техника категориясининг тури</label>
                                                <select id="vehicle_category_type" class="form-control" name="vehicle_category_type" required>
                                                    @foreach (\App\Models\VehicleType::all() as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label" for="name">Товар Сопроводительные документы</label>
                                                <input type="number" class="form-control" name="invoice_number"
                                                       placeholder="Товар Сопроводительные документы" value="" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label" for="name">Товар Сопроводительные документы Дата</label>
                                                <input type="date" class="form-control" name="invoice_date" placeholder="Товар Сопроводительные документы Дата"
                                                       value="" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="files" style="padding: 20px; background-color: #9ce2fd; cursor:pointer; border-radius: 5px;">Texnika rasmini tanlash uchun bosing</label>
                                                <input type="file" style="display: none" name="images[]" id="files" multiple accept="image/jpeg, image/png, image/gif,"><br />
                                                </span>
                                                <output id="Filelist">

                                                    <ul class="thumb-Images">

                                                        @foreach(json_decode($application->image) as $image)
                                                            <li>
                                                                <div class="img-wrap">
                                                                    <span class="close">×</span>
                                                                    <img src="{{asset($image)}}" class="thumb" alt="">
                                                                </div>
                                                            </li>

                                                        @endforeach
                                                    </ul>

                                                </output>
                                            </div>
                                        </div>
                                        <div class="form-group  col-md-3 ">

                                            <label class="control-label" for="name">File1</label>
                                            <input type="file" name="file1[]" multiple="multiple">
                                        </div>
                                        <div class="form-group  col-md-3 ">

                                            <label class="control-label" for="name">File 2</label>
                                            <input type="file" name="file2[]" multiple="multiple">
                                        </div>
                                        <div class="form-group  col-md-3 ">

                                            <label class="control-label" for="name">File 3</label>
                                            <input type="file" name="file3[]" multiple="multiple">
                                        </div>
                                        <div class="form-group  col-md-3 ">

                                            <label class="control-label" for="name">File 4</label>
                                            <input type="file" name="file4[]" multiple="multiple">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Yuborish</button>
                            </div>
                            <iframe id="form_target" name="form_target" style="display:none"></iframe>

                            {{--                        <form id="my_form" action="http://reestr.local/admin/upload" target="form_target" method="post"--}}
                            {{--                              enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">--}}
                            {{--                            <input name="image" id="upload_file" type="file"--}}
                            {{--                                   onchange="$('#my_form').submit();this.value='';">--}}
                            {{--                            <input type="hidden" name="type_slug" id="type_slug" value="applications">--}}
                            {{--                            <input type="hidden" name="_token" value="hF3AsgcgnVr3yvbRmafXEbQ3KcLUiLsrUQ7jydD3">--}}
                            {{--                        </form>--}}
                        </div>
                    </div>
                </div>
            </div>
        </form>



        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            //I added event handler for the file upload control to access the files properties.
            document.addEventListener("DOMContentLoaded", init, false);

            //To save an array of attachments
            var AttachmentArray = [];

            //counter for attachment array
            var arrCounter = 0;

            //to make sure the error message for number of files will be shown only one time.
            var filesCounterAlertStatus = false;

            //un ordered list to keep attachments thumbnails
            var ul = document.createElement("ul");
            ul.className = "thumb-Images";
            ul.id = "imgList";

            function init() {
                //add javascript handlers for the file upload event
                document
                    .querySelector("#files")
                    .addEventListener("change", handleFileSelect, false);
            }

            //the handler for file upload event
            function handleFileSelect(e) {
                //to make sure the user select file/files
                if (!e.target.files) return;

                //To obtaine a File reference
                var files = e.target.files;

                // Loop through the FileList and then to render image files as thumbnails.
                var lis = document.querySelectorAll("#imgList li");

                for (var i = 0, f; (f = files[i]); i++) {
                    //instantiate a FileReader object to read its contents into memory
                    var fileReader = new FileReader();

                    // Closure to capture the file information and apply validation.
                    fileReader.onload = (function(readerEvt) {
                        return function(e) {
                            //Apply the validation rules for attachments upload
                            ApplyFileValidationRules(readerEvt);

                            //Render attachments thumbnails.
                            RenderThumbnail(e, readerEvt);

                            //Fill the array of attachment
                            FillAttachmentArray(e, readerEvt);
                        };
                    })(f);

                    // Read in the image file as a data URL.
                    // readAsDataURL: The result property will contain the file/blob's data encoded as a data URL.
                    // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
                    var lis = document.querySelectorAll("#imgList li");
                    lis.innerHTML == ""

                    fileReader.readAsDataURL(f);
                }
                document
                    .getElementById("files")
                    .addEventListener("change", handleFileSelect, false);
            }

            //To remove attachment once user click on x button
            jQuery(function($) {
                $("div").on("click", ".img-wrap .close", function() {
                    var id = $(this)
                        .closest(".img-wrap")
                        .find("img")
                        .data("id");

                    //to remove the deleted item from array
                    var elementPos = AttachmentArray.map(function(x) {
                        return x.FileName;
                    }).indexOf(id);
                    if (elementPos !== -1) {
                        AttachmentArray.splice(elementPos, 1);
                    }

                    //to remove image tag
                    $(this)
                        .parent()
                        .find("img")
                        .not()
                        .remove();

                    //to remove div tag that contain the image
                    $(this)
                        .parent()
                        .find("div")
                        .not()
                        .remove();

                    //to remove div tag that contain caption name
                    $(this)
                        .parent()
                        .parent()
                        .find("div")
                        .not()
                        .remove();

                    //to remove li tag
                    var lis = document.querySelectorAll("#imgList li");
                    for (var i = 0; (li = lis[i]); i++) {
                        if (li.innerHTML == "") {
                            li.parentNode.removeChild(li);
                        }
                    }
                });
            });

            //Apply the validation rules for attachments upload
            function ApplyFileValidationRules(readerEvt) {
                //To check file type according to upload conditions
                if (CheckFileType(readerEvt.type) == false) {
                    alert(
                        "The file (" +
                        readerEvt.name +
                        ") does not match the upload conditions, You can only upload jpg/png/gif files"
                    );
                    e.preventDefault();
                    return;
                }

                //To check file Size according to upload conditions
                if (CheckFileSize(readerEvt.size) == false) {
                    alert(
                        "The file (" +
                        readerEvt.name +
                        ") does not match the upload conditions, The maximum file size for uploads should not exceed 300 KB"
                    );
                    e.preventDefault();
                    return;
                }

                //To check files count according to upload conditions
                if (CheckFilesCount(AttachmentArray) == false) {
                    if (!filesCounterAlertStatus) {
                        filesCounterAlertStatus = true;
                        alert(
                            "You have added more than 10 files. According to upload conditions you can upload 10 files maximum"
                        );
                    }
                    e.preventDefault();
                    return;
                }
            }

            //To check file type according to upload conditions
            function CheckFileType(fileType) {
                if (fileType == "image/jpeg") {
                    return true;
                } else if (fileType == "image/png") {
                    return true;
                } else if (fileType == "image/gif") {
                    return true;
                } else {
                    return false;
                }
                return true;
            }

            // To check file Size according to upload conditions
            function CheckFileSize(fileSize) {
                if (fileSize < 300000000 ) {
                    return true;
                } else {
                    return false;
                }
                return true;
            }

            // To check files count according to upload conditions
            function CheckFilesCount(AttachmentArray) {
                //Since AttachmentArray.length return the next available index in the array,
                //I have used the loop to get the real length
                var len = 0;
                for (var i = 0; i < AttachmentArray.length; i++) {
                    if (AttachmentArray[i] !== undefined) {
                        len++;
                    }
                }
                //To check the length does not exceed 10 files maximum
                if (len > 900000) {
                    return false;
                } else {
                    return true;
                }
            }

            //Render attachments thumbnails.
            function RenderThumbnail(e, readerEvt) {
                var li = document.createElement("li");
                ul.appendChild(li);
                li.innerHTML = [
                    '<div class="img-wrap"> <span class="close">&times;</span>' +
                    '<img class="thumb" src="',
                    e.target.result,
                    '" title="',
                    escape(readerEvt.name),
                    '" data-id="',
                    readerEvt.name,
                    '"/>' + "</div>"
                ].join("");

                var div = document.createElement("div");
                div.className = "FileNameCaptionStyle";
                li.appendChild(div);
                div.innerHTML = [readerEvt.name].join("");
                document.getElementById("Filelist").insertBefore(ul, null);
            }

            //Fill the array of attachment
            function FillAttachmentArray(e, readerEvt) {
                AttachmentArray[arrCounter] = {
                    AttachmentType: 1,
                    ObjectType: 1,
                    FileName: readerEvt.name,
                    FileDescription: "Attachment",
                    NoteText: "",
                    MimeType: readerEvt.type,
                    Content: e.target.result.split("base64,")[1],
                    FileSizeInBytes: readerEvt.size
                };
                arrCounter = arrCounter + 1;
            }

        </script>
        <script>

            // Manually add a file using the addfile method

            var uppy = new Uppy.Core()
                .use(Uppy.Dashboard, {
                    inline: true,
                    target: '#drag-drop-area'
                })
                .use(Uppy.XHRUpload, { endpoint: '/fileUpload',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }})

            uppy.on('complete', (result) => {
                console.log('Upload complete! We’ve uploaded these files:', result.successful)
            })
        </script>
        <!-- End Delete File Modal -->
@endsection
















{{--@extends('voyager::master')--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>--}}
{{--<link href="https://releases.transloadit.com/uppy/v2.1.1/uppy.min.css" rel="stylesheet">--}}
{{--<style>--}}
{{--    .slider.round {--}}
{{--        border-radius: 17px;--}}
{{--    }--}}

{{--    .slider.round:before {--}}
{{--        border-radius: 50%;--}}
{{--    }.switch {--}}
{{--         position: relative;--}}
{{--         display: inline-block;--}}
{{--         width: 30px;--}}
{{--         height: 17px;--}}
{{--     }--}}

{{--    /* Hide default HTML checkbox */--}}
{{--    .switch input {--}}
{{--        opacity: 0;--}}
{{--        width: 0;--}}
{{--        height: 0;--}}
{{--    }--}}

{{--    /* The slider */--}}
{{--    .slider {--}}
{{--        position: absolute;--}}
{{--        cursor: pointer;--}}
{{--        top: 0;--}}
{{--        left: 0;--}}
{{--        right: 0;--}}
{{--        bottom: 0;--}}
{{--        background-color: #ccc;--}}
{{--        -webkit-transition: .4s;--}}
{{--        transition: .4s;--}}
{{--    }--}}

{{--    .slider:before {--}}
{{--        position: absolute;--}}
{{--        content: "";--}}
{{--        height: 13px;--}}
{{--        width: 13px;--}}
{{--        left: 2px;--}}
{{--        bottom: 2px;--}}
{{--        background-color: white;--}}
{{--        -webkit-transition: .4s;--}}
{{--        transition: .4s;--}}
{{--    }--}}

{{--    input:checked + .slider {--}}
{{--        background-color: #2196F3;--}}
{{--    }--}}

{{--    input:focus + .slider {--}}
{{--        box-shadow: 0 0 1px #2196F3;--}}
{{--    }--}}

{{--    input:checked + .slider:before {--}}
{{--        -webkit-transform: translateX(13px);--}}
{{--        -ms-transform: translateX(13px);--}}
{{--        transform: translateX(13px);--}}
{{--    }--}}
{{--</style>--}}
{{--<style type="text/css">--}}
{{--    #regiration_form fieldset:not(:first-of-type) {--}}
{{--        display: none;--}}
{{--    }--}}
{{--</style>--}}
{{--@section('content')--}}
{{--    <h1 class="page-title">--}}
{{--        <i class=""></i>Add Application--}}
{{--    </h1>--}}
{{--    <div id=" voyager-notifications">--}}
{{--        <div class="page-content edit-add container-fluid">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-12">--}}

{{--                    <div class="panel panel-bordered">--}}
{{--                        <!-- form start -->--}}
{{--                        <form role="form" class="form-edit-add" action="{{route('voyager.applicationnnn.create')}}"--}}
{{--                              method="POST" enctype="multipart/form-data">--}}
{{--                            <!-- PUT Method if we are editing -->--}}

{{--                            <!-- CSRF TOKEN -->--}}
{{--                            @csrf--}}

{{--                            <div class="panel-body">--}}
{{--                                <div class="form-group  col-md-12 ">--}}
{{--                                    <label class="control-label" for="import">Qaysi turdagi tovar uchun?</label><br>--}}
{{--                                    <select class="form-control " name="user_type" onchange="usertype(this.value)">--}}
{{--                                        <option value="0">O'zbekistonda ishlab chiqarilgan tovar</option>--}}
{{--                                        <option value="1">Import/Eksport</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}


{{--                                <div class="form-group  col-md-12 ">--}}
{{--                                    <label class="control-label" for="name">TNVED Raqami</label>--}}
{{--                                    <select id="company" class="form-control" name="Type">--}}
{{--                                        @foreach (\App\Models\TnvedCode::all() as $type)--}}
{{--                                            <option value="{{ $type->id }}">{{ $type->code }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}

{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name">Ariza beruvchi</label>--}}
{{--                                    <select class="form-control " name="user_type" onchange="usertype(this.value)">--}}
{{--                                        <option value="0">Jismoniy shaxs</option>--}}
{{--                                        <option value="1">Yuridik shaxs</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="company">Tashkilot</label>--}}


{{--                                    <select id="company" class="form-control" name="company_id">--}}
{{--                                        @foreach (auth()->user()->companies as $type)--}}
{{--                                            <option value="{{ $type->id }}">{{ $type->name }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}


{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name">Texnika nomi</label>--}}
{{--                                    <input type="text" class="form-control" name="vehicle_name" placeholder="Texnika nomi"--}}
{{--                                           value="">--}}


{{--                                </div>--}}


{{--                                <div class="form-group  col-md-12 ">--}}
{{--                                    <label class="control-label" for="manafactured_country">Ishlab chiqaruvchi davlat</label>--}}
{{--                                    --}}{{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
{{--                                    <select id="manafactured_country" class="form-control" name="manafactured_country">--}}
{{--                                        @foreach (\App\Models\countries::all() as $type)--}}
{{--                                            <option value="{{ $type->id }}">{{ $type->name   }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="form-group  col-md-12 ">--}}
{{--                                    <label class="control-label" for="sender_company">Tovarni jo'natuvchi tashkilot</label>--}}
{{--                                    <select id="sender_company" class="form-control" name="sender_company">--}}
{{--                                        @foreach(App\Models\Gtk::all() as $gtk)--}}
{{--                                            <option value="{{$gtk->G8_NAME}}" data-gtk="{{$gtk->id}}">{{$gtk->G8_NAME}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="form-group  col-md-12 ">--}}
{{--                                    <label class="control-label" for="quantity">Soni</label>--}}
{{--                                    <select id="quantity" class="form-control" name="quantity">--}}
{{--                                        @foreach(App\Models\Tovar::all() as $tovar)--}}
{{--                                            <option value="{{$tovar->QUANTITY}}">{{$tovar->QUANTITY}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="form-group  col-md-12 ">--}}
{{--                                    <label class="control-label" for="import_country">Import qiluvchi davlat</label>--}}
{{--                                    --}}{{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
{{--                                    <select id="import_country" class="form-control" name="import_country">--}}
{{--                                        @foreach (\App\Models\countries::all() as $type)--}}
{{--                                            <option value="{{ $type->id }}">{{ $type->name   }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="form-group  col-md-12 ">--}}
{{--                                    <label class="control-label" for="name">Ishlab chiqaruvchi davlat</label>--}}
{{--                                    --}}{{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
{{--                                    <select id="country" class="form-control" name="manufactured_country">--}}
{{--                                        @foreach (\App\Models\countries::all() as $type)--}}
{{--                                            <option value="{{ $type->id }}">{{ $type->name   }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}

{{--                                <div class="form-group  col-md-12 ">--}}
{{--                                    <label class="control-label" for="vehicle_type">Texnika turi</label>--}}
{{--                                    --}}{{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
{{--                                    <select id="vehicle_type" class="form-control">--}}
{{--                                        @foreach (\App\Models\VehicleType::all() as $type)--}}
{{--                                            <option value="{{ $type->id }}">{{ $type->name }}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}

{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="vehicle_category_type">Texnika kategoriyasining turi</label>--}}
{{--                                    <input type="vehicle_category_type" class="form-control" name="vehicle_category_type">--}}

{{--                                </div>--}}



{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name"> </label>--}}
{{--                                    <input type="hidden" name="image" accept="image/*">--}}
{{--                                </div>--}}


{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}

{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name">Товар Сопроводительные документы</label>--}}
{{--                                    <input type="text" class="form-control" name="invoice_number"--}}
{{--                                           placeholder="Товар Сопроводительные документы" value="">--}}
{{--                                </div>--}}
{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}

{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name">Товар Сопроводительные документы Дата</label>--}}
{{--                                    <input type="text" class="form-control" name="invoice_date" placeholder="Товар Сопроводительные документы Дата"--}}
{{--                                           value="">--}}

{{--                                    <div id="drag-drop-area"></div>--}}
{{--                                </div>--}}
{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}

{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name"></label>--}}
{{--                                    <input type="hidden" name="file1[]" multiple="multiple">--}}


{{--                                </div>--}}
{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}

{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name"></label>--}}
{{--                                    <input type="hidden" name="file2[]" multiple="multiple">--}}


{{--                                </div>--}}
{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}

{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name"></label>--}}
{{--                                    <input type="hidden" name="file3[]" multiple="multiple">--}}


{{--                                </div>--}}
{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}

{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name"></label>--}}
{{--                                    <input type="hidden" name="file4[]" multiple="multiple">--}}


{{--                                </div>--}}
{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}
{{--                                <input type="hidden" id="sel_type" value="">--}}
{{--                                <div class="form-group  col-md-12 ">--}}

{{--                                    <label class="control-label" for="name">Ariza beruvchi</label>--}}
{{--                                    <input class="form-control" type="text" disabled value="{{ Auth::user()->fullname }}" />--}}






{{--                                </div>--}}

{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}


{{--                                <!-- GET THE DISPLAY OPTIONS -->--}}


{{--                            </div><!-- panel-body -->--}}

{{--                            <div class="panel-footer">--}}
{{--                                <button type="submit" class="btn btn-primary save">Save</button>--}}
{{--                            </div>--}}
{{--                        </form>--}}

{{--                        <iframe id="form_target" name="form_target" style="display:none"></iframe>--}}
{{--                        <form id="my_form" action="http://reestr.local/admin/upload" target="form_target" method="post"--}}
{{--                              enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">--}}
{{--                            <input name="image" id="upload_file" type="file"--}}
{{--                                   onchange="$('#my_form').submit();this.value='';">--}}
{{--                            <input type="hidden" name="type_slug" id="type_slug" value="applications">--}}
{{--                            <input type="hidden" name="_token" value="hF3AsgcgnVr3yvbRmafXEbQ3KcLUiLsrUQ7jydD3">--}}
{{--                        </form>--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <script>--}}
{{--            function usertype(ch) {--}}
{{--                company = document.getElementById('company');--}}

{{--                if (ch == 1) {--}}
{{--                    company.disabled = false;--}}
{{--                } else--}}
{{--                    company.disabled = true;--}}

{{--            }--}}

{{--            function imported() {--}}

{{--                imported = document.getElementById('import');--}}
{{--                if(imported.value() == "import"){--}}

{{--                }--}}
{{--            }--}}


{{--        </script>--}}
{{--        <div class="modal fade modal-danger" id="confirm_delete_modal">--}}
{{--            <div class="modal-dialog">--}}
{{--                <div class="modal-content">--}}

{{--                    <div class="modal-header">--}}
{{--                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
{{--                        <h4 class="modal-title"><i class="voyager-warning"></i> Are you sure</h4>--}}
{{--                    </div>--}}

{{--                    <div class="modal-body">--}}
{{--                        <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>--}}
{{--                    </div>--}}

{{--                    <div class="modal-footer">--}}
{{--                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>--}}
{{--                        <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <script src="https://releases.transloadit.com/uppy/v2.1.1/uppy.min.js"></script>--}}

{{--        <script>--}}
{{--            var uppy = new Uppy.Core()--}}
{{--                .use(Uppy.Dashboard, {--}}
{{--                    inline: true,--}}
{{--                    target: '#drag-drop-area'--}}
{{--                })--}}
{{--                .use(Uppy.XHRUpload, { endpoint: '/fileUpload',--}}
{{--                    headers: {--}}
{{--                        'X-CSRF-TOKEN': '{{ csrf_token() }}'--}}
{{--                    }})--}}

{{--            uppy.on('complete', (result) => {--}}
{{--                console.log('Upload complete! We’ve uploaded these files:', result.successful)--}}
{{--            })--}}
{{--        </script>--}}
{{--        <!-- End Delete File Modal -->--}}
{{--@endsection--}}
