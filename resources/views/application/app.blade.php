@extends('voyager::master')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<style type="text/css">
    #regiration_form fieldset:not(:first-of-type) {
        display: none;
    }

</style>
@section('content')
        <h1 class="page-title">
            <i class=""></i>Add Application</h1>
            <div id=" voyager-notifications">
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" class="form-edit-add" action="http://reestr.local/admin/applications" method="POST"
                        enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->

                        <!-- CSRF TOKEN -->
                        <input type="hidden" name="_token" value="hF3AsgcgnVr3yvbRmafXEbQ3KcLUiLsrUQ7jydD3">

                        <div class="panel-body">
                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">TNVED Raqami</label>
                                <select class="form-control select2-ajax select2-hidden-accessible" name="tnved_code"
                                    data-get-items-route="http://reestr.local/admin/applications/relation"
                                    data-get-items-field="application_belongsto_tnved_code_relationship_1" data-method="add"
                                    required="" data-select2-id="1" tabindex="-1" aria-hidden="true">


                                </select>
                            </div>
                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">Ariza beruvchi</label>
                                <select class="form-control " name="user_type" onchange="usertype(this.value)">
                                    <option value="0">Jismoniy shaxs</option>
                                    <option value="1">Yuridik shaxs</option>
                                </select>
                            </div>
                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">Tashkilot</label>
                                {{-- <select id="company" disabled class="form-control select2-ajax select2-hidden-accessible" name="company_id"
                                    data-get-items-route="http://reestr.local/admin/applications/relation"
                                    data-get-items-field="application_belongsto_company_relationship" data-method="add"
                                    data-select2-id="9" tabindex="-1" aria-hidden="true">

                                    <option value="" data-select2-id="11">None</option>

                                </select> --}}
                                @php
                                    $user_companies = App\Models\UserCompany::where('user_id', Auth::user()->id)->get();
                                    foreach ($user_companies as $value) {
                                        $companies[] = $value->company_id;
                                    }
                                @endphp
                                {{-- <select id="company" class="form-control" name="company_id" disabled>
                                    @foreach (\App\Models\Company::whereIn("id", $companies)->get() as $company)
                                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                                    @endforeach
                                </select> --}}
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">Product Type</label>
                                <input type="text" class="form-control" name="product_type" placeholder="Product Type"
                                    value="">


                            </div>


                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">Vehicle Name</label>
                                <input type="text" class="form-control" name="vehicle_name" placeholder="Vehicle Name"
                                    value="">


                            </div>
                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">Ishlab chiqaruvchi</label>
                                <select class="form-control select2-ajax select2-hidden-accessible"
                                    name="manafactured_country"
                                    data-get-items-route="http://reestr.local/admin/applications/relation"
                                    data-get-items-field="application_belongsto_country_relationship" data-method="add"
                                    data-select2-id="12" tabindex="-1" aria-hidden="true">

                                    <option value="" data-select2-id="14">None</option>

                                </select>
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="name">Texnika turi</label>
                                {{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
                                <select id="company" class="form-control" name="Type">
                                    @foreach (\App\Models\VehicleType::all() as $type)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">Image</label>
                                <input type="file" name="image" accept="image/*">


                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">Invoice Number</label>
                                <input type="text" class="form-control" name="invoice_number" placeholder="Invoice Number"
                                    value="">


                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">Invoice Date</label>
                                <input type="text" class="form-control" name="invoice_date" placeholder="Invoice Date"
                                    value="">


                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">File1</label>
                                <input type="file" name="file1[]" multiple="multiple">


                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">File2</label>
                                <input type="file" name="file2[]" multiple="multiple">


                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">File3</label>
                                <input type="file" name="file3[]" multiple="multiple">


                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">File4</label>
                                <input type="file" name="file4[]" multiple="multiple">


                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->
                            <input type="hidden" id="sel_type" value="">
                            <div class="form-group  col-md-12 ">

                                <label class="control-label" for="name">Ariza beruvchi</label>
                                <input class="form-control" type="text" disabled value="{{ Auth::user()->fullname }}"/>






                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->


                            <!-- GET THE DISPLAY OPTIONS -->


                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">Save</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="http://reestr.local/admin/upload" target="form_target" method="post"
                        enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="applications">
                        <input type="hidden" name="_token" value="hF3AsgcgnVr3yvbRmafXEbQ3KcLUiLsrUQ7jydD3">
                    </form>

                </div>
            </div>
        </div>
    </div>
    <script>
        function usertype(ch){
            company = document.getElementById('company');

            if(ch == 1){
                company.disabled = false;
            } else
            company.disabled = true;

        }
        function selected(s) {


        }

    </script>
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> Are you sure</h4>
                </div>

                <div class="modal-body">
                    <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@endsection
