@extends('voyager::master')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="https://releases.transloadit.com/uppy/v2.1.1/uppy.min.css" rel="stylesheet">
<style>
    .slider.round {
        border-radius: 17px;
    }

    .slider.round:before {
        border-radius: 50%;
    }.switch {
         position: relative;
         display: inline-block;
         width: 30px;
         height: 17px;
     }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 13px;
        width: 13px;
        left: 2px;
        bottom: 2px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(13px);
        -ms-transform: translateX(13px);
        transform: translateX(13px);
    }
</style>
<style type="text/css">
    #regiration_form fieldset:not(:first-of-type) {
        display: none;
    }
</style>
@section('content')
    <h1 class="page-title">
        <i class=""></i>Payment
    </h1>
    <div id=" voyager-notifications">
        <div class="page-content edit-add container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-bordered">
                        <!-- form start -->
                        <form role="form" class="form-edit-add" action="{{route('applications.update')}}"
                              method="POST" enctype="multipart/form-data">
                            <!-- PUT Method if we are editing -->

                            <!-- CSRF TOKEN -->
                            @csrf
                            <div class="panel-body">
                                <!-- GET THE DISPLAY OPTIONS -->




                                    </select>
                                </div>
                                <div class="form-group  col-md-12 ">
                                    <label class="control-label" for="import_country">Status</label>
                                    {{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
                                    <select id="import_country" class="form-control" name="import_country" required>
                                        @foreach ($statuses = ["Новый","В рассмотрении", "Принято на исполнение", "Завершен","Аннулирован",  "Просрочен"] as $status)
                                            <option value="">{{ $status}}</option>
                                        @endforeach
                                    </select>
                                </div>






                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"> </label>
                                    <input type="hidden" name="image" accept="image/*" required>
                                </div>


                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name">Товар Сопроводительные документы</label>
                                    <input type="number" class="form-control" name="invoice_number"
                                           placeholder="Товар Сопроводительные документы" value="" required>
                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name">Товар Сопроводительные документы Дата</label>
                                    <input type="text" class="form-control" name="invoice_date" placeholder="Товар Сопроводительные документы Дата"
                                           value="" required>

                                    <div id="drag-drop-area"></div>
                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"></label>
                                    <input type="hidden" name="file1[]" multiple="multiple">


                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"></label>
                                    <input type="hidden" name="file2[]" multiple="multiple">


                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"></label>
                                    <input type="hidden" name="file3[]" multiple="multiple">


                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"></label>
                                    <input type="hidden" name="file4[]" multiple="multiple">


                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->
                                <input type="hidden" id="sel_type" value="">
                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name">Ariza beruvchi</label>
                                    <input class="form-control" type="text" disabled value="{{ Auth::user()->fullname }}" />






                                </div>

                                <!-- GET THE DISPLAY OPTIONS -->


                                <!-- GET THE DISPLAY OPTIONS -->


                            </div><!-- panel-body -->

                            <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Save</button>
                            </div>
                        </form>

                        <iframe id="form_target" name="form_target" style="display:none"></iframe>
                        <form id="my_form" action="http://reestr.local/admin/upload" target="form_target" method="post"
                              enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                            <input name="image" id="upload_file" type="file"
                                   onchange="$('#my_form').submit();this.value='';">
                            <input type="hidden" name="type_slug" id="type_slug" value="applications">
                            <input type="hidden" name="_token" value="hF3AsgcgnVr3yvbRmafXEbQ3KcLUiLsrUQ7jydD3">
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade modal-danger" id="confirm_delete_modal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"><i class="voyager-warning"></i> Are you sure</h4>
                    </div>

                    <div class="modal-body">
                        <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!</button>
                    </div>
                </div>
            </div>
        </div>
@endsection
