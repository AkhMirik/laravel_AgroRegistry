@extends('voyager::master')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="https://releases.transloadit.com/uppy/v2.1.1/uppy.min.css" rel="stylesheet">
<style>
    .slider.round {
        border-radius: 17px;
    }

    .slider.round:before {
        border-radius: 50%;
    }.switch {
         position: relative;
         display: inline-block;
         width: 30px;
         height: 17px;
     }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 13px;
        width: 13px;
        left: 2px;
        bottom: 2px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(13px);
        -ms-transform: translateX(13px);
        transform: translateX(13px);
    }
</style>
<style type="text/css">
    #regiration_form fieldset:not(:first-of-type) {
        display: none;
    }
</style>
@section('content')
    <h1 class="page-title">
        <i class=""></i>Add Application
    </h1>
    <div id=" voyager-notifications">
        <div class="page-content edit-add container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-bordered">
                        <!-- form start -->
                        <form role="form" class="form-edit-add" action="{{route('applications.update')}}"
                              method="POST" enctype="multipart/form-data">
                            <!-- PUT Method if we are editing -->

                            <!-- CSRF TOKEN -->
                            @csrf
                            <div class="panel-body">
                                <div class="form-group  col-md-12 ">
                                    <label class="control-label" for="import">Qaysi turdagi tovar uchun?</label><br>
                                    <select class="form-control " name="user_type" onchange="usertype(this.value)" required>
                                        <option value="0">O'zbekistonda ishlab chiqarilgan tovar</option>
                                        <option value="1">Import/Eksport</option>
                                    </select>
                                </div>


                                <div class="form-group  col-md-12">
                                    <label class="control-label" for="tnved_code">TNVED Raqami</label>
                                    <select id="tnved_code" class="form-control" name="tnved_code" required>
                                        @foreach (\App\Models\TnvedCode::all() as $type)
                                            <option value="{{ $type->id }}">{{ $type->code }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name">Ariza beruvchi</label>
                                    <select class="form-control " name="user_type" onchange="usertype(this.value)" required>
                                        <option value="0">Jismoniy shaxs</option>
                                        <option value="1">Yuridik shaxs</option>
                                    </select>
                                </div>

                                @if(count(auth()->user()->companies))
                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="company">Tashkilot</label>


                                    <select id="company" class="form-control" name="company_id" required>
                                        @foreach (auth()->user()->companies as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @endif
                                <!-- GET THE DISPLAY OPTIONS -->


                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name">Texnika nomi</label>
                                    <input type="text" class="form-control" name="vehicle_name" placeholder="Texnika nomi" required
                                           value="">


                                </div>



                                <div class="form-group  col-md-12 ">
                                    <label class="control-label" for="sender_company">Tovarni jo'natuvchi tashkilot</label>
                                    <select id="sender_company" class="form-control" name="sender_company" required>
                                        @foreach(App\Models\Gtk::all() as $gtk)
                                            <option value="{{$gtk->G8_NAME}}" data-gtk="{{$gtk->id}}">{{$gtk->G8_NAME}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group  col-md-12 ">
                                    <label class="control-label" for="quantity">Soni</label>
                                    <input type="number" id="quantity" class="form-control" name="quantity" required>
                                        <!-- @foreach(App\Models\Tovar::all() as $tovar)
                                            <option value="{{$tovar->QUANTITY}}">{{$tovar->QUANTITY}}</option>
                                        @endforeach -->
                                    </input>
                                </div>
                                <div class="form-group  col-md-12 ">
                                    <label class="control-label" for="import_country">Import qiluvchi davlat</label>
                                    {{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
                                    <select id="import_country" class="form-control" name="import_country" required>
                                        @foreach (\App\Models\countries::all() as $type)
                                            <option value="{{ $type->id }}">{{ $type->name   }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group  col-md-12 ">
                                    <label class="control-label" for="manafactured_country">Ishlab chiqaruvchi davlat</label>
                                    {{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
                                    <select id="manafactured_country" class="form-control" name="manafactured_country" required>
                                        @foreach (\App\Models\countries::all() as $type)
                                            <option value="{{ $type->id }}">{{ $type->name   }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">
                                    <label class="control-label" for="vehicle_category">Texnika turi</label>
                                    {{-- <input type="text" class="form-control" name="Category" placeholder="Category" value=""> --}}
                                    <select id="vehicle_category" class="form-control" name="vehicle_category" required>
                                        @foreach (\App\Models\VehicleType::all() as $type)
                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="vehicle_category_type">Texnika kategoriyasining turi</label>
                                    <input type="text" class="form-control" name="vehicle_category_type" required>

                                </div>



                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"> </label>
                                    <input type="hidden" name="image" accept="image/*" required>
                                </div>


                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name">Товар Сопроводительные документы</label>
                                    <input type="number" class="form-control" name="invoice_number"
                                           placeholder="Товар Сопроводительные документы" value="" required>
                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name">Товар Сопроводительные документы Дата</label>
                                    <input type="text" class="form-control" name="invoice_date" placeholder="Товар Сопроводительные документы Дата"
                                           value="" required>

                                    <div id="drag-drop-area"></div>
                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"></label>
                                    <input type="hidden" name="file1[]" multiple="multiple">


                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"></label>
                                    <input type="hidden" name="file2[]" multiple="multiple">


                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"></label>
                                    <input type="hidden" name="file3[]" multiple="multiple">


                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->

                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name"></label>
                                    <input type="hidden" name="file4[]" multiple="multiple">


                                </div>
                                <!-- GET THE DISPLAY OPTIONS -->
                                <input type="hidden" id="sel_type" value="">
                                <div class="form-group  col-md-12 ">

                                    <label class="control-label" for="name">Ariza beruvchi</label>
                                    <input class="form-control" type="text" disabled value="{{ Auth::user()->fullname }}" />






                                </div>

                                <!-- GET THE DISPLAY OPTIONS -->


                                <!-- GET THE DISPLAY OPTIONS -->


                            </div><!-- panel-body -->

                            <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Save</button>
                            </div>
                        </form>

                        <iframe id="form_target" name="form_target" style="display:none"></iframe>
                        <form id="my_form" action="http://reestr.local/admin/upload" target="form_target" method="post"
                              enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                            <input name="image" id="upload_file" type="file"
                                   onchange="$('#my_form').submit();this.value='';">
                            <input type="hidden" name="type_slug" id="type_slug" value="applications">
                            <input type="hidden" name="_token" value="hF3AsgcgnVr3yvbRmafXEbQ3KcLUiLsrUQ7jydD3">
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade modal-danger" id="confirm_delete_modal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"><i class="voyager-warning"></i> Are you sure</h4>
                    </div>

                    <div class="modal-body">
                        <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!</button>
                    </div>
                </div>
            </div>
        </div>
@endsection
