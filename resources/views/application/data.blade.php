@extends('voyager::master')


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>

    /*Copied from bootstrap to handle input file multiple*/
    h4{
        min-height: 30px;
    }
    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    /*Also */
    .btn-success {
        border: 1px solid #c5dbec;
        background: #d0e5f5;
        font-weight: bold;
        color: #2e6e9e;
    }
    /* This is copied from https://github.com/blueimp/jQuery-File-Upload/blob/master/css/jquery.fileupload.css */
    .fileinput-button {
        position: relative;
        overflow: hidden;
    }

    .fileinput-button input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        opacity: 0;
        -ms-filter: "alpha(opacity=0)";
        font-size: 200px;
        direction: ltr;
        cursor: pointer;
    }

    .thumb {
        height: 100px;


    }

    ul.thumb-Images li {
        width: 120px;
        float: left;
        display: inline-block;
        vertical-align: top;
        height: 120px;
        background-color: #6aa2ec;
        border-radius: 10px;
        margin: 10px;
        padding: 10px;
    }

    .img-wrap {
        position: relative;
        display: inline-block;
        font-size: 0;
        overflow: hidden;
        width: 100%;
        height: 100%;
        margin-bottom: 20px;
    }
    .FileNameCaptionStyle{
        width: 100%;
        overflow: hidden;
    }

    .img-wrap .close {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
        background-color: #d0e5f5;
        padding: 5px 2px 2px;
        color: #000;
        font-weight: bolder;
        cursor: pointer;
        opacity: 0.5;
        font-size: 23px;
        line-height: 10px;
        border-radius: 50%;
    }
    .close{
        height: 20px;
        width: 20px;
        text-align: center;
        transition: 0.2s;
    }
    .img-wrap:hover .close {
        opacity: 1;
        background-color: #ff0000;
        color: white;
    }

    .FileNameCaptionStyle {
        font-size: 12px;
    }


    body{
        color: black!important;
    }
    .slider.round {
        border-radius: 17px;
    }

    .slider.round:before {
        border-radius: 50%;
    }.switch {
         position: relative;
         display: inline-block;
         width: 30px;
         height: 17px;
     }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 13px;
        width: 13px;
        left: 2px;
        bottom: 2px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(13px);
        -ms-transform: translateX(13px);
        transform: translateX(13px);
    }
    .user{
        font-size: 18px;
        width: 90%;
        cursor: pointer;
        font-weight: 500;
        padding: 10px;
        margin-left: 10px;
        border: 1px solid transparent;
        border-radius: 5px;
    }
    .user:focus{
        border-color: gray;
    }
    .curs{
        cursor: pointer;
    }
</style>
<style type="text/css">
    #regiration_form fieldset:not(:first-of-type) {
        display: none;
    }
</style>
@section('content')
    <h1 class="page-title">
        <i class=""></i>Add Application
    </h1>

    <div id=" voyager-notifications">
        <form role="form" class="form-edit-add" action="{{route('voyager.application.check')}}"
              method="POST" enctype="multipart/form-data">
            @csrf

            <div class="page-content edit-add container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h1>Қишлоқ хўжалигидаги техникаларни синовдан ўтказишга ариза бериш </h1>

                                        </div>
                                    </div>
                                    @if($status == "data" || $status == "data_uz")
                                        <div class="row">
                                            <div class="col-lg-3">Қайси турдаги товар учун:</div>
                                            <div class="col-lg-9"><h4>{{$tovar_type}}</h4></div>
                                            <div class="col-lg-3">ТН ВЭД КОД:</div>
                                            <div class="col-lg-9"><h4>{{$tnved_code->code}}</h4></div>
                                            <div class="col-lg-3">Техника категорияси:</div>
                                            <div class="col-lg-9"><h4>{{$tnved_code->name}}</h4></div>
                                        </div>

                                        <input type="hidden" name="tovar_type" value="{{$tovar_type}}">
                                        <input type="hidden" name="tnved_code" value="{{$tnved_code}}">
                                    @endif
                                    @if($status == "data")
                                        <div class="row">
                                            <input type="hidden" name="gtk" value="{{$gtk->id}}">
                                            <input type="hidden" name="tovar" value="{{$tovar->id}}">
                                            <input type="hidden" name="vehicle_name" value="{{$tovar->G31NAME}}">
                                            <input type="hidden" name="quantity" value="{{$tovar->QUANTITY}}">
                                            <div class="col-lg-3">Қабул қилувчи давлат:</div>
                                            <div class="col-lg-9"><h4>{{$gtk->G17}}</h4></div>

                                            <div class="col-lg-3">Жўнатувчи давлат:</div>
                                            <div class="col-lg-9"><h4>{{$gtk->G15}}</h4></div>

                                            <div class="col-lg-3">Товарни жўнатувчи ташкилот номи:</div>
                                            <div class="col-lg-9"><h4>{{$gtk->G2_NAME}}</h4></div>

                                            <div class="col-lg-3">Техника номи:</div>
                                            <div class="col-lg-9"><h4>{{$tovar->G31NAME}}</h4></div>

                                            <div class="col-lg-3">Сони:</div>
                                            <div class="col-lg-9"><h4>{{$tovar->QUANTITY}}</h4></div>
                                       </div>
                                        @elseif($status == "data_uz")
                                            <div class="row">
                                                <div class="col-lg-3">Техника Номи:</div>
                                                <div class="col-lg-9"><input type="text" required class="user" style="border-color: gray" name="vehicle_name" placeholder="Техника номини киритинг"></div>
                                                <div class="col-lg-3">Сони:</div>
                                                <div class="col-lg-9"><input type="number" required class="user" style="border-color: gray" name="quantity" placeholder="Техника сонини киритинг"></div>

                                            </div>
                                        @elseif($status == "no_data")
                                            <h2 class="text-center">Хеч қандай маълумот топилмади!</h2>

                                        <div class="col-lg-1">
                                            <a href="{{route('voyager.applications.user_next')}}" class="btn btn-primary">Ортга</a>
                                        </div>
                                        @endif


                                    @if($status == "data" || $status == "data_uz")

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="files" style="padding: 20px; background-color: #9ce2fd; cursor:pointer; border-radius: 5px;">Texnika rasmini tanlash uchun bosing</label>
                                                    <input type="file" style="display: none" name="images[]" id="files" multiple accept="image/jpeg, image/png, image/gif,"><br />
                                                    <output id="Filelist"></output>
                                                </div>
                                            </div>
                                            <div class="form-group  col-md-3 ">

                                                <label class="control-label" for="name">File1</label>
                                                <input type="file" name="file1[]" multiple="multiple">
                                            </div>
                                            <div class="form-group  col-md-3 ">

                                                <label class="control-label" for="name">File 2</label>
                                                <input type="file" name="file2[]" multiple="multiple">
                                            </div>
                                            <div class="form-group  col-md-3 ">

                                                <label class="control-label" for="name">File 3</label>
                                                <input type="file" name="file3[]" multiple="multiple">
                                            </div>
                                            <div class="form-group  col-md-3 ">

                                                <label class="control-label" for="name">File 4</label>
                                                <input type="file" name="file4[]" multiple="multiple">
                                            </div>
                                            <div class="col-lg-1">
                                                <a href="{{route('voyager.applications.user_next')}}" class="btn btn-primary">Ортга</a>
                                            </div>

                                            <div class="col-lg-1">
                                                <button name="finish" class="btn btn-primary">Давом этиш</button>
                                            </div>
                                        </div>

                                    @endif

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <script>
            //I added event handler for the file upload control to access the files properties.
            document.addEventListener("DOMContentLoaded", init, false);

            //To save an array of attachments
            var AttachmentArray = [];

            //counter for attachment array
            var arrCounter = 0;

            //to make sure the error message for number of files will be shown only one time.
            var filesCounterAlertStatus = false;

            //un ordered list to keep attachments thumbnails
            var ul = document.createElement("ul");
            ul.className = "thumb-Images";
            ul.id = "imgList";

            function init() {
                //add javascript handlers for the file upload event
                document
                    .querySelector("#files")
                    .addEventListener("change", handleFileSelect, false);
            }

            //the handler for file upload event
            function handleFileSelect(e) {
                //to make sure the user select file/files
                if (!e.target.files) return;

                //To obtaine a File reference
                var files = e.target.files;

                // Loop through the FileList and then to render image files as thumbnails.
                for (var i = 0, f; (f = files[i]); i++) {
                    //instantiate a FileReader object to read its contents into memory
                    var fileReader = new FileReader();

                    // Closure to capture the file information and apply validation.
                    fileReader.onload = (function(readerEvt) {
                        return function(e) {
                            //Apply the validation rules for attachments upload
                            ApplyFileValidationRules(readerEvt);

                            //Render attachments thumbnails.
                            RenderThumbnail(e, readerEvt);

                            //Fill the array of attachment
                            FillAttachmentArray(e, readerEvt);
                        };
                    })(f);

                    // Read in the image file as a data URL.
                    // readAsDataURL: The result property will contain the file/blob's data encoded as a data URL.
                    // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
                    fileReader.readAsDataURL(f);
                }
                document
                    .getElementById("files")
                    .addEventListener("change", handleFileSelect, false);
            }

            //To remove attachment once user click on x button
            jQuery(function($) {
                $("div").on("click", ".img-wrap .close", function() {
                    var id = $(this)
                        .closest(".img-wrap")
                        .find("img")
                        .data("id");

                    //to remove the deleted item from array
                    var elementPos = AttachmentArray.map(function(x) {
                        return x.FileName;
                    }).indexOf(id);
                    if (elementPos !== -1) {
                        AttachmentArray.splice(elementPos, 1);
                    }

                    //to remove image tag
                    $(this)
                        .parent()
                        .find("img")
                        .not()
                        .remove();

                    //to remove div tag that contain the image
                    $(this)
                        .parent()
                        .find("div")
                        .not()
                        .remove();

                    //to remove div tag that contain caption name
                    $(this)
                        .parent()
                        .parent()
                        .find("div")
                        .not()
                        .remove();

                    //to remove li tag
                    var lis = document.querySelectorAll("#imgList li");
                    for (var i = 0; (li = lis[i]); i++) {
                        if (li.innerHTML == "") {
                            li.parentNode.removeChild(li);
                        }
                    }
                });
            });

            //Apply the validation rules for attachments upload
            function ApplyFileValidationRules(readerEvt) {
                //To check file type according to upload conditions
                if (CheckFileType(readerEvt.type) == false) {
                    alert(
                        "The file (" +
                        readerEvt.name +
                        ") does not match the upload conditions, You can only upload jpg/png/gif files"
                    );
                    e.preventDefault();
                    return;
                }

                //To check file Size according to upload conditions
                if (CheckFileSize(readerEvt.size) == false) {
                    alert(
                        "The file (" +
                        readerEvt.name +
                        ") does not match the upload conditions, The maximum file size for uploads should not exceed 300 KB"
                    );
                    e.preventDefault();
                    return;
                }

                //To check files count according to upload conditions
                if (CheckFilesCount(AttachmentArray) == false) {
                    if (!filesCounterAlertStatus) {
                        filesCounterAlertStatus = true;
                        alert(
                            "You have added more than 10 files. According to upload conditions you can upload 10 files maximum"
                        );
                    }
                    e.preventDefault();
                    return;
                }
            }

            //To check file type according to upload conditions
            function CheckFileType(fileType) {
                if (fileType == "image/jpeg") {
                    return true;
                } else if (fileType == "image/png") {
                    return true;
                } else if (fileType == "image/gif") {
                    return true;
                } else {
                    return false;
                }
                return true;
            }

            // To check file Size according to upload conditions
            function CheckFileSize(fileSize) {
                if (fileSize < 300000000 ) {
                    return true;
                } else {
                    return false;
                }
                return true;
            }

            // To check files count according to upload conditions
            function CheckFilesCount(AttachmentArray) {
                //Since AttachmentArray.length return the next available index in the array,
                //I have used the loop to get the real length
                var len = 0;
                for (var i = 0; i < AttachmentArray.length; i++) {
                    if (AttachmentArray[i] !== undefined) {
                        len++;
                    }
                }
                //To check the length does not exceed 10 files maximum
                if (len > 900000) {
                    return false;
                } else {
                    return true;
                }
            }

            //Render attachments thumbnails.
            function RenderThumbnail(e, readerEvt) {
                var li = document.createElement("li");
                ul.appendChild(li);
                li.innerHTML = [
                    '<div class="img-wrap"> <span class="close">&times;</span>' +
                    '<img class="thumb" src="',
                    e.target.result,
                    '" title="',
                    escape(readerEvt.name),
                    '" data-id="',
                    readerEvt.name,
                    '"/>' + "</div>"
                ].join("");

                var div = document.createElement("div");
                div.className = "FileNameCaptionStyle";
                li.appendChild(div);
                div.innerHTML = [readerEvt.name].join("");
                document.getElementById("Filelist").insertBefore(ul, null);
            }

            //Fill the array of attachment
            function FillAttachmentArray(e, readerEvt) {
                AttachmentArray[arrCounter] = {
                    AttachmentType: 1,
                    ObjectType: 1,
                    FileName: readerEvt.name,
                    FileDescription: "Attachment",
                    NoteText: "",
                    MimeType: readerEvt.type,
                    Content: e.target.result.split("base64,")[1],
                    FileSizeInBytes: readerEvt.size
                };
                arrCounter = arrCounter + 1;
            }

        </script>


@endsection
