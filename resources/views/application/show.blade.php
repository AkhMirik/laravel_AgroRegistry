@extends('voyager::master')


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>

    /*Copied from bootstrap to handle input file multiple*/
    h4{
        min-height: 30px;
    }
    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    /*Also */
    .btn-success {
        border: 1px solid #c5dbec;
        background: #d0e5f5;
        font-weight: bold;
        color: #2e6e9e;
    }
    /* This is copied from https://github.com/blueimp/jQuery-File-Upload/blob/master/css/jquery.fileupload.css */
    .fileinput-button {
        position: relative;
        overflow: hidden;
    }

    .fileinput-button input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        opacity: 0;
        -ms-filter: "alpha(opacity=0)";
        font-size: 200px;
        direction: ltr;
        cursor: pointer;
    }

    .thumb {
        height: 100px;


    }

    ul.thumb-Images li {
        width: 120px;
        float: left;
        display: inline-block;
        vertical-align: top;
        height: 120px;
        background-color: #6aa2ec;
        border-radius: 10px;
        margin: 10px;
        padding: 10px;
    }

    .img-wrap {
        position: relative;
        display: inline-block;
        font-size: 0;
        overflow: hidden;
        width: 100%;
        height: 100%;
        margin-bottom: 20px;
    }
    .FileNameCaptionStyle{
        width: 100%;
        overflow: hidden;
    }

    .img-wrap .close {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
        background-color: #d0e5f5;
        padding: 5px 2px 2px;
        color: #000;
        font-weight: bolder;
        cursor: pointer;
        opacity: 0.5;
        font-size: 23px;
        line-height: 10px;
        border-radius: 50%;
    }
    .close{
        height: 20px;
        width: 20px;
        text-align: center;
        transition: 0.2s;
    }
    .img-wrap:hover .close {
        opacity: 1;
        background-color: #ff0000;
        color: white;
    }

    .FileNameCaptionStyle {
        font-size: 12px;
    }


    body{
        color: black!important;
    }
    .slider.round {
        border-radius: 17px;
    }

    .slider.round:before {
        border-radius: 50%;
    }.switch {
         position: relative;
         display: inline-block;
         width: 30px;
         height: 17px;
     }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 13px;
        width: 13px;
        left: 2px;
        bottom: 2px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(13px);
        -ms-transform: translateX(13px);
        transform: translateX(13px);
    }
    .user{
        font-size: 18px;
        width: 90%;
        cursor: pointer;
        font-weight: 500;
        padding: 10px;
        margin-left: 10px;
        border: 1px solid transparent;
        border-radius: 5px;
    }
    .user:focus{
        border-color: gray;
    }
    .curs{
        cursor: pointer;
    }
</style>
<style type="text/css">
    #regiration_form fieldset:not(:first-of-type) {
        display: none;
    }
</style>
@section('content')
    <div id=" voyager-notifications">
            <div class="page-content edit-add container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h1>Қишлоқ хўжалигидаги техникаларни синовдан ўтказишга ариза бериш </h1>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3">
                                            Ариза холати:
                                        </div>
                                        <div class="col-lg-9">
                                            <h4> {{$status}}</h4>
                                        </div>
                                        @if($application->user)
                                        <div class="col-lg-3">
                                            Ариза берувчи шахс:
                                        </div>
                                        <div class="col-lg-9">
                                            <h4> {{$application->user->fullname}}</h4>
                                        </div>
                                        <div class="col-lg-3">
                                            Ариза берувчи мақоми:
                                        </div>
                                        <div class="col-lg-9">
                                            <h4>{{$application->user->user_type}}</h4>
                                        </div>
                                        @if(count($application->user->companies))
                                            <div class="col-lg-3">
                                                Ташкилот:
                                            </div>
                                            <div class="col-lg-10">
                                                <h4>
                                                    {{$application->user->companies->first()->name}}
                                                </h4>
                                            </div>
                                        @endif
                                        @endif
                                        <div class="col-lg-3">Телефон:</div>
                                        <div class="col-lg-9 d-flex"><h4>{{$application->user->phone}}</h4></div>
                                        <div class="col-lg-3">Электрон почта:</div>
                                        <div class="col-lg-9 d-flex"><h4>{{$application->user->email}}</h4></div>
                                        <div class="col-lg-3">Доимий яшаш жойи:</div>
                                        <div class="col-lg-9 d-flex"><h4>{{$application->user->address}}</h4></div>

                                        <div class="col-lg-3">Қайси турдаги товар учун:</div>
                                        <div class="col-lg-9"><h4>{{$application->tovar_type}}</h4></div>
                                        <div class="col-lg-3">ТН ВЭД КОД:</div>
                                        <div class="col-lg-9"><h4>{{$tnved_code->code}}</h4></div>
                                        <div class="col-lg-3">Техника категорияси:</div>
                                        <div class="col-lg-9"><h4>{{$tnved_code->name}}</h4></div>
                                        <div class="col-lg-3">Техника Номи:</div>
                                        <div class="col-lg-9"><h4>{{$application->vehicle_name}}</h4></div>

                                        @if(session('status')=="data")

                                            <div class="col-lg-3">Қабул қилувчи давлат:</div>
                                            <div class="col-lg-9"><h4>{{$application->import_country}}</h4></div>

                                            <div class="col-lg-3">Жўнатувчи давлат:</div>
                                            <div class="col-lg-9"><h4>{{$application->manafactured_country}}</h4></div>

                                            <div class="col-lg-3">Товарни жўнатувчи ташкилот номи:</div>
                                            <div class="col-lg-9"><h4>{{$application->sender_company}}</h4></div>
                                        @endif


                                        <div class="col-lg-3">Техника Номи:</div>
                                        <div class="col-lg-9"><h4>{{$application->quantity}}</h4></div>
                                        <div class="col-lg-12">
                                            <ul class="thumb-Images">
                                                @forelse(\GuzzleHttp\json_decode($application->image) as $image)
                                                    <li>
                                                        <div class="img-wrap">
                                                            <img src="{{asset($image)}}" class="thumb" alt="">
                                                        </div>
                                                    </li>

                                                @endforeach
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="col-lg-1">
                                        <a href="{{route('voyager.applications.index')}}" class="btn btn-primary">Ортга</a>
                                    </div>
                                    @if(auth()->user()->role_id == 10)
                                        <div class="col-lg-1">
                                            <a href="{{route('voyager.applications.protocol', $application->id)}}" class="btn btn-primary">Protocol</a>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

@endsection
