@extends('voyager::master')

    @section('content')

        <div class="side-body padding-top">
                        <h1 class="page-title">
            <i class=""></i>
            Добавить Contract
        </h1>
                        <div id="voyager-notifications"></div>
                        <div class="page-content edit-add container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-bordered">
                        <!-- form start -->
                        <form role="form" class="form-edit-add" action="http://reestr.test/admin/contracts" method="POST" enctype="multipart/form-data">
                            <!-- PUT Method if we are editing -->

                            <!-- CSRF TOKEN -->
                            <input type="hidden" name="_token" value="A9iJdrfPXnsARvQQVsoCI9MGFlPyXXiCkcDH8bRB">

                            <div class="panel-body">


                                <!-- Adding / Editing -->

                                                                <!-- GET THE DISPLAY OPTIONS -->
                                                                <meta property="og:description" content="gbgbjgbgj">
                                    <div class="form-group  col-md-12 ">

                                        <label class="control-label" for="name">Name</label>
                                                                                                                    <input required="" type="text" class="form-control" name="name" placeholder="Name" value="">


                                                                                                            </div>
                                                                <!-- GET THE DISPLAY OPTIONS -->

                                    <div class="form-group  col-md-12 ">

                                        <label class="control-label" for="name">Inn</label>
                                                                                                                    <input type="text" class="form-control" name="inn" placeholder="Inn" value="">


                                                                                                            </div>
                                                                <!-- GET THE DISPLAY OPTIONS -->

                                    <div class="form-group  col-md-12 ">

                                        <label class="control-label" for="name">Pinfl</label>
                                                                                                                    <input type="text" class="form-control" name="pinfl" placeholder="Pinfl" value="">


                                                                                                            </div>
                                                                <!-- GET THE DISPLAY OPTIONS -->

                                    <div class="form-group  col-md-12 ">

                                        <label class="control-label" for="name">Location</label>
                                                                                                                    <input type="text" class="form-control" name="location" placeholder="Location" value="">


                                                                                                            </div>
                                                                <!-- GET THE DISPLAY OPTIONS -->

                                    <div class="form-group  col-md-12 ">

                                        <label class="control-label" for="name">Phone</label>
                                                                                                                    <input type="text" class="form-control" name="phone" placeholder="Phone" value="">


                                                                                                            </div>
                                                                <!-- GET THE DISPLAY OPTIONS -->

                                    <div class="form-group  col-md-12 ">

                                        <label class="control-label" for="name">Contract No</label>
                                                                                                                    <input type="text" class="form-control" name="contract_no" placeholder="Contract No" value="">


                                                                                                            </div>
                                                                <!-- GET THE DISPLAY OPTIONS -->

                                    <div class="form-group  col-md-12 ">

                                        <label class="control-label" for="name">Expire Date</label>
                                                                                                                    <input type="datetime" class="form-control datepicker" name="expire_date" value="">


                                                                                                            </div>
                                                                <!-- GET THE DISPLAY OPTIONS -->

                                    <div class="form-group  col-md-12 ">

                                        <label class="control-label" for="name">Contract Price</label>
                                                                                                                    <input type="text" class="form-control" name="contract_price" placeholder="Contract Price" value="">


                                                                                                            </div>
                                                                <!-- GET THE DISPLAY OPTIONS -->

                                    <div class="form-group  col-md-12 ">


                                        <label class="control-label" for="name">Deleted At</label>
                                                                                                                    <input type="datetime" class="form-control datepicker" name="deleted_at" value="">


                                                                                                            </div>

                            </div><!-- panel-body -->

                            <div class="panel-footer">
                                                                                            <button type="submit" class="btn btn-primary save">Сохранить </button>
                                                        </div>
                        </form>

                        <iframe id="form_target" name="form_target" style="display:none"></iframe>
                        <form id="my_form" action="http://reestr.test/admin/upload" target="form_target" method="post" enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                            <input type="hidden" name="type_slug" id="type_slug" value="contracts">
                            <input type="hidden" name="_token" value="A9iJdrfPXnsARvQQVsoCI9MGFlPyXXiCkcDH8bRB">
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade modal-danger" id="confirm_delete_modal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"><i class="voyager-warning"></i> Вы уверены </h4>
                    </div>

                    <div class="modal-body">
                        <h4>Вы точно хотите удалить  '<span class="confirm_delete_name"></span>'</h4>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена </button>
                        <button type="button" class="btn btn-danger" id="confirm_delete">Да, удалить! </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Delete File Modal -->
                </div>

@endsection
