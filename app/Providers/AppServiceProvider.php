<?php

namespace App\Providers;

use App\View\Components\navbar;
use App\View\Components\footer;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
    */
    public function boot()
    {
        Voyager::addAction(\App\Actions\wordAction::class);
        Voyager::addAction(\App\Actions\payAction::class);
        Voyager::addAction(\App\Actions\LabaratoryAction::class);
        Voyager::addAction(\App\Actions\ActiveAction::class);
        Blade::component('navbar', navbar::class);
        Blade::component('footer', footer::class);
        Schema::defaultStringLength(191);
    }
}
