<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Laravel\Scout\Searchable;

class Application extends Model
{
    use HasFactory,Searchable;

    protected $fillable = ['status'];

	public function scopeCurrentUser($query)
    {
        // return Auth::user()->hasRole('admin') ? $query : (Auth::user()->hasRole('moderator') ? $query->where('responsible', Auth::user()->id) : $query->where('user_id', Auth::user()->id));
        // $routes = Routes::where('responsible', Auth::user()->id)->get();
        return (Auth::user()->hasRole('admin') || Auth::user()->hasRole('laboratory') || Auth::user()->hasRole('accountant')) ? $query : $query->where('user_id', Auth::user()->id);
    }

//    public function getImageAttribute($value){
//        $json = [];
//        $file_json = json_decode($value);
//        foreach ($file_json as $json_val){
//            $json[] = "storage/".$json_val;
//        }
//        return $json;
//    }

    public function user(){
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function company(){
        return $this->hasMany(Company::class);
    }
}
