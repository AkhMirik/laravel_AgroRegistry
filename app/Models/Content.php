<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Content extends Model
{
    use HasFactory,Searchable;



    public static function param_name($application_id,$param_id, $i){
        $param_name = "param_".$param_id."_".$i;
        $content = Content::query()->where('application_id', $application_id)->where('parametr_id', $param_id, $i)->first();

        return $content?json_decode($content->inputs)->$param_name:null;
    }
}
