<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Gtk extends Model
{
    use HasFactory,Searchable;


    public function tovars(){
        return $this->hasMany(Tovar::class);
    }
}
