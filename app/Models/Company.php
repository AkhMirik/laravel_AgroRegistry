<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Laravel\Scout\Searchable;

class Company extends Model
{
    use HasFactory,Searchable;
    protected $fillable = [
        'name',
        'stir'
    ];
    protected $rules = [
        'name' => 'required',
        'stir' => 'unique'
    ];
    public function scopeCurrentCompany($query)
    {
        // return Auth::user()->hasRole('admin') ? $query : (Auth::user()->hasRole('moderator') ? $query->where('responsible', Auth::user()->id) : $query->where('user_id', Auth::user()->id));
        // $routes = Routes::where('responsible', Auth::user()->id)->get();
        $companies = UserCompany::where('user_id', Auth::user()->id)->get()->pluck('company_id');
        return Auth::user()->hasRole('admin') ? $query : $query->whereIn('id', $companies);
    }
}
