<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Application;
use App\Models\ApplyingForTesting;
use App\Models\Contract;
use App\Models\Countries;
use App\Models\Department;
use App\Models\District;
use App\Models\Gost;
use App\Models\Gtk;
use App\Models\GtkTitle;
use App\Models\Labaratory;
use App\Models\Parameter;
use App\Models\Region;
use App\Models\TnvedCode;
use App\Models\Tovar;
use App\Models\User;
use App\Models\UserCompany;
use App\Models\Vehicle;
use App\Models\VehicleType;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class VoyagerSearchController extends VoyagerBaseController
{
     public function indexSearch () {
         return view('search');
    }

    public function indexForm() {
        return view('form');
    }

    public function ShowSearch(Request $request) {
        $users = User::search($request->searchQueryInput)->get();
        $Application = Application::search($request->searchQueryInput)->get();
        $tovar = Tovar::search($request->searchQueryInput)->get();
        $country = Countries::search($request->searchQueryInput)->get();
        $userCompany = UserCompany::search($request->searchQueryInput)->get();
        $department = Department::search($request->searchQueryInput)->get();
        $region = Region::search($request->searchQueryInput)->get();
        $address = Address::search($request->searchQueryInput)->get();
        $Gtk = Gtk::search($request->searchQueryInput)->get();
        $GtkTitle = GtkTitle::search($request->searchQueryInput)->get();
        $Labaratory = Labaratory::search($request->searchQueryInput)->get();
        $address = Parameter::search($request->searchQueryInput)->get();
        $address = Vehicle::search($request->searchQueryInput)->get();
        $address = VehicleType::search($request->searchQueryInput)->get();
        $address = Gost::search($request->searchQueryInput)->get();
        $address = TnvedCode::search($request->searchQueryInput)->get();
        $address = ApplyingForTesting::search($request->searchQueryInput)->get();
        $address = District::search($request->searchQueryInput)->get();
        $address = Contract::search($request->searchQueryInput)->get();




        dd($users,$Application,$tovar,$country,$userCompany,$department,$region,$address);
    }
}
