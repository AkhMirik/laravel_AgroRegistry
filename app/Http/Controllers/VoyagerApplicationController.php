<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerController;

class VoyagerApplicationController extends VoyagerController
{
    public function index(){

    }

    public function show(){

    }

    public function create(){

    }

    public function store(Request $request){

    }

    public function edit(){
        return view('voyager::tools.bread.edit-add');
    }

    public function update(Request $request){

    }
    public function destroy(Request $request){

    }
}
