<?php

namespace App\Actions;

use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Actions\AbstractAction;

class payAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Paid';
    }

    public function getIcon()
    {
        return 'voyager-credit-card';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right',
            'id' => 'confirm_warning_modal',
            'onclick' => 'warningModal()'
        ];
    }

    public function getDefaultRoute()
    {
        return route('applications.pay',  ['id' => $this->data->id]);
    }
    public function shouldActionDisplayOnDataType()
    {
        if (Auth::user()->role->name == "accountant") {
            return $this->dataType->slug == 'applications';
        }
    }
}
