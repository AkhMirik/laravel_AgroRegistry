<?php
namespace App\Actions;


use TCG\Voyager\Actions\AbstractAction;
use Illuminate\Support\Facades\Auth;
class  ActiveAction extends AbstractAction {
    public function getTitle(){
        return "activate";
    }
    public function getIcon(){
        return 'voyager-download';
    }
    public function getPolicy(){
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-danger pull right',
        ];
    }

    public function getDefaultRoute()
    {
        // , compact('appeal_id')
        return route('users.activity', ['user' => $this->data->id]);
    }

    public function shouldActionDisplayOnDataType()
    {
//        if(Auth::user()->hasRole('user')){
        return $this->dataType->slug == 'users';
//        }
    }

}
