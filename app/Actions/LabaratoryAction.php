<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;
use Illuminate\Support\Facades\Auth;

class  LabaratoryAction extends AbstractAction {
    public function getTitle(){
        return "Labaratory";
    }
    public function getIcon(){
        return 'voyager-download';
    }
    public function getPolicy(){
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull right',

        ];
    }

    public function getDefaultRoute()
    {
        // , compact('appeal_id')
        return route('labaratory.test', ['application' => $this->data->id]);
    }

    public function shouldActionDisplayOnDataType()
    {
        if(Auth::user()->hasRole('laboratory')){
            return $this->dataType->slug == 'applications';

        }
    }

}
